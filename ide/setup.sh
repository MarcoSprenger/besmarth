#!/bin/bash
echo Setting up development environment...
cp ./IP34-19vt_BNE.iml ../.idea/IP34-19vt_BNE.iml
cp ./misc.xml ../.idea/misc.xml
cp ./codeStyles/Project.xml ../.idea/Project.xml
cp ./codeStyles/codeStyleConfig.xml ../.idea/codeStyles/codeStyleConfig.xml


cp ./runConfigurations/Sustainable_Behaviour_Backend_Tests.xml ../.idea/runConfigurations/Sustainable_Behaviour_Backend_Tests.xml
cp ./runConfigurations/Sustainable_Behaviour_Backend.xml ../.idea/runConfigurations/Sustainable_Behaviour_Backend.xml
cp ./runConfigurations/Sustainable_Behaviour_App_Tests.xml ../.idea/runConfigurations/Sustainable_Behaviour_App_Tests.xml
cp ./runConfigurations/Sustainable_Behaviour_App.xml ../.idea/runConfigurations/Sustainable_Behaviour_App.xml

echo Finished