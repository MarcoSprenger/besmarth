@echo off
echo Setting up development environment...
COPY .\IP34-19vt_BNE.iml ..\.idea\IP34-19vt_BNE.iml
COPY .\misc.xml ..\.idea\misc.xml
COPY .\codeStyles\Project.xml ..\.idea\Project.xml
COPY .\codeStyles\codeStyleConfig.xml ..\.idea\codeStyles\codeStyleConfig.xml


COPY .\runConfigurations\Sustainable_Behaviour_Backend_Tests.xml ..\.idea\runConfigurations\Sustainable_Behaviour_Backend_Tests.xml
COPY .\runConfigurations\Sustainable_Behaviour_Backend.xml ..\.idea\runConfigurations\Sustainable_Behaviour_Backend.xml
COPY .\runConfigurations\Sustainable_Behaviour_App_Tests.xml ..\.idea\runConfigurations\Sustainable_Behaviour_App_Tests.xml
COPY .\runConfigurations\Sustainable_Behaviour_App.xml ..\.idea\runConfigurations\Sustainable_Behaviour_App.xml

echo Finished