import React from "react";
import ManageAccountScreen from "../ManageAccountScreen";
import {
  createEmptyTestStore,
  createNetworkMock,
  renderScreenForTest,
  waitForReduxAction
} from "../../../test/test-utils";
import {waitForElement} from "@testing-library/react-native";
import * as SecureStore from "expo-secure-store";

const mock = createNetworkMock();

jest.mock("../../../services/phone-utils.js", () => {
    return {
      isPhoneOnline: () => Promise.resolve(false),
      isConnectedToWifi: () => Promise.resolve(false),
    };
  }
);

jest.mock("expo-secure-store", () => ({
  getItemAsync: jest.fn()
}));

describe("ManageAccountScreen-offline", () => {
  it(`renders with cached account`, async () => {
    SecureStore.getItemAsync.mockImplementation(() => JSON.stringify({
      username: "test",
      email: "test@email.com",
      first_name: "Samuel",
      pseudonymous: false
    }));
    const store = createEmptyTestStore();
    const {asJSON, getByLabelText} = renderScreenForTest(ManageAccountScreen, store);
    await waitForReduxAction("ACCOUNT_UPDATE");
    await waitForElement(() => getByLabelText("Vorname"));
    expect(asJSON()).toMatchSnapshot();
  });

  it(`shows error when offline and no cached account available`, async () => {
    SecureStore.getItemAsync.mockImplementation(() => null);

    const store = createEmptyTestStore();
    const {asJSON, getByTestId} = renderScreenForTest(ManageAccountScreen, store);

    await waitForElement(() => getByTestId("Fehleranzeige"));
    expect(asJSON()).toMatchSnapshot();
  });
});