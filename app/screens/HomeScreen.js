import React from "react";
import {
    Dimensions,
    SafeAreaView,
    StyleSheet,
    Text,
    View,
    ScrollView
} from "react-native";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import MapItem from "../components/MapItem";
import {fetchTopics} from "../services/Topics";
import {
    Card,
    Paragraph,
} from "react-native-paper";

class HomeScreen extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            loading: true
        };
    }

    componentDidMount() {
        this.reload();
    }

    async reload() {
        this.props.fetchTopics();
    }

    render() {
        let welcomeInfo;

        welcomeInfo =   <Paragraph style={{margin: 15, marginTop: 15}}>
                            Schön, dass du dich für eine nachhaltigere und gerechtere Welt interessierst. Hier kannst du einen Bereich auswählen, den du in den nächsten vier Wochen nachhaltiger gestalten möchtest.
                        </Paragraph>;

        const topics = this.props.topics;
        if (topics.length > 0) {
            const mapViews = topics.map(topic => {
                return <View style={styles.tile} key={topic.internal_id}>
                    <Card style={{elevation: 10}}>
                        <MapItem topic={topic} key={topic.internal_id}/>
                    </Card>
                </View>;
            });

            return (
                <ScrollView style={{flex: 1}}>
                    {welcomeInfo}
                    <SafeAreaView style={styles.container}>
                        {mapViews}
                    </SafeAreaView>
                    <Paragraph/>
                </ScrollView>
            );
        } else {
            return (
                <View style={styles.container}>
                    <Text>Es sind keine Topics vorhanden.</Text>
                </View>
            );
        }
    }
}

const {width, height} = Dimensions.get("window");
const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexWrap: "wrap",
        flexDirection: "row",
    },
    tile: {
        height: height / 5,
        width: width / 2.4,
        margin: 15,
        paddingBottom: 10
    },
});

const mapStateToProps = state => {
    return {
        topics: Object.values(state.topics.topics),
        refreshing: state.topics.refreshing,
    };
};

const mapDispatchToProps = dispatch =>
    bindActionCreators({
        fetchTopics,
    }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(HomeScreen);