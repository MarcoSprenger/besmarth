import {createEmptyTestStore, renderScreenForTest} from "../../../test/test-utils";
import AccountCodeScreen from "../AccountCodeScreen";
import {fireEvent, getByLabelText, waitForElement} from "@testing-library/react-native";
import Navigation from "../../../services/Navigation";

describe("AccountCodeScreen", () => {
  it(`renders`, async () => {
    const {asJSON} = renderScreenForTest(AccountCodeScreen, createEmptyTestStore());
    expect(asJSON()).toMatchSnapshot();
  });

  it(`back button works`, async () => {
    const {asJSON, getByLabelText} = renderScreenForTest(AccountCodeScreen, createEmptyTestStore());
    const backBtn = await waitForElement(() => getByLabelText('Zurück'));
    fireEvent.press(backBtn);
    expect(Navigation.goBack.mock.calls.length).toBe(1);
  });
});