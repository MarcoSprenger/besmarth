import React from "react";
import {AppRegistry, Dimensions, FlatList, RefreshControl, ScrollView, StyleSheet, View} from "react-native";
import {bindActionCreators, combineReducers} from "redux";
import {connect} from "react-redux";
import {Button, Headline, IconButton, Paragraph, Searchbar, TextInput, Text, TouchableRipple, Divider} from "react-native-paper";
import Navigation from "../../services/Navigation";
import {showToast} from "../../services/Toast";
import SegmentedControlTab from "react-native-segmented-control-tab";
import {
    acceptFriendRequest,
    createFriendRequest,
    deleteFriend,
    deleteFriendRequest,
    fetchFriends,
    fetchIncomingFriendRequests,
    fetchOutgoingFriendRequests,
    searchUser
} from "../../services/FriendService";
import debounce from "debounce";
import {notNull} from "../../services/utils";
import {Ionicons} from "@expo/vector-icons";
import {Colors} from "../../styles/index";
import {
    createGroup,
    createGroupAffiliation,
    createGroupInvitation,
    fetchGroups,
    fetchGroupInvitations,
    fetchGroupAffiliations,
} from "../../services/GroupsharingService";
import { fetchSharedContent, fetchLikedContent, createLikedContent,  } from "../../services/SharedContentService";

// Index Vor Tabbed View
const SEARCH_VIEW = 0;
const REQUEST_INCOMING_VIEW = 1;
const REQUEST_OUTGOING_VIEW = 2;

// Values for User Type Map
const UT_FRIEND = "friend";
const UT_FRIEND_REQUEST_SENT = "request_sent";
const UT_FRIEND_REQUEST_INCOMING = "request_incoming";

class SharingOverviewScreen extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            loading: true,
            selectedIndex: SEARCH_VIEW,
            friendSearch: "",
            friends: [],
            strangers: [],
            groupSearch: "",
            own_groups: [],
            unaffiliated_groups: [],

            new_group_name:"",
            new_group_password:"",

            join_group_name:"",
            join_group_password:"",

            new_member: [],

            content: [],
            groups: []
        };
    }

    // Setting the index of SegmentedControlTab
    handleIndexChange = index => {
        this.setState({
            ...this.state,
            selectedIndex: index
        });
    };


    /**
     * Redirects the user to SignIn screen if he is not signed in
     */
    componentDidMount() {
        if (this.props.signedIn) {
            this.props.navigation.setOptions({
                headerRight: () => (
                    <IconButton
                        onPress={() => Navigation.navigate("FriendsTab", "AccountCode")}
                        accessibilityLabel="Eigenen QR-Code anzeigen"
                        icon="qrcode"
                        testID="toggle-qrcode"
                        color="white"
                        size={20}/>
                )
            });
        }

        this.navFocusListenerSub = this.props.navigation.addListener("focus", this.onScreenFocus.bind(this));
    }

    componentWillUnmount() {
        this.navFocusListenerSub();
    }

    /**
     * Called when the user focuses this screen
     */
    async onScreenFocus() {
        if (notNull(this.props?.route?.params?.scannedCode)) {
            this.onSearch(this.props?.route?.params?.scannedCode);
            this.props.navigation.setParams({scannedCode: null});
            // do not reload, this is already done by onSearch
        } else {
            this.reload().then();
        }
    }

    async reload() {
        if (!this.props.signedIn) {
            // do not load anything if not signed in
            return;
        }
        this.setState({
            ...this.state,
            loading: true,
        });
        this.searchUsersImmediately(this.state.friendSearch);
    }

    //DEL
    openScanner() {
        Navigation.navigate("FriendsTab", "ScanFriendCode");
    }

    //DEL
    async onSearch(value) {
        this.setState({
            ...this.state,
            friendSearch: value,
        });

        // searchUsers will be delayed for 300ms, to reduce calls to searchUsers
        await this.searchUsers(value);
    }

    // immediately fires off a reload
    searchUsersImmediately = async (username) => {
        username = username.toLocaleLowerCase();
        try {

            // Get Friends and co. data
            const responseFriends = await fetchFriends();
            const responseRequestSent = await fetchOutgoingFriendRequests();
            const responseRequestIncoming = await fetchIncomingFriendRequests();

            // Get Groups, Affiliations and Invitations + Shared Content Stuff
            const responseGroups = await fetchGroups();
            const responseSharedContents = await fetchSharedContent();

            /*const responseGroupInvitations = await fetchGroupInvitations(); 
            const responseGroupAffiliations = await fetchGroupAffiliations();
            const responseLikedContent = await fetchLikedContent()
            */

            // Data Lists for Friends and co.
            const friends = responseFriends.data.filter(s => s.username.toLocaleLowerCase().includes(username));
            const requestSent = responseRequestSent.data.map(o => o.receiver);
            const requestIncoming = responseRequestIncoming.data.map(o => o.sender);

            groups = responseGroups.data.map(g => g.group_name);
            content = responseSharedContents.data
            


            // Populate Friends and co. in userTypeMap
            const userTypeMap = new Map();

            friends.forEach((f) => userTypeMap.set(f.id, UT_FRIEND));
            requestSent.forEach((u) => userTypeMap.set(u.id, UT_FRIEND_REQUEST_SENT));
            requestIncoming.forEach((u) => userTypeMap.set(u.id, UT_FRIEND_REQUEST_INCOMING));

            let strangers = [];
            if (username.length >= 3) {
                const searchResponse = await searchUser(username);
                // Remove friends and logged in user
                strangers = searchResponse.data.filter(s => {
                        const isFriend = userTypeMap.get(s.id) !== UT_FRIEND;
                        const isCurrentUser = s.username !== this.props.username;
                        return isFriend && isCurrentUser;
                    }
                );
            }

            this.setState({
                ...this.state,
                loading: false,
                error: null,
                friends,
                strangers,
                requestSent,
                requestIncoming,
                userTypeMap,
                groups,
                content
            });

            //console.log(this.state.content);

        } catch (e) {
            this.setState({
                    error: JSON.stringify(e),
                    loading: false
                }
            );
        }
    };


    // to be called by search,  defers actual reload for 300ms
    searchUsers = debounce(this.searchUsersImmediately, 300);

    async sendFriendRequest(userId) {
        try {
            await createFriendRequest(userId);
            this.props.showToast("Gruppenanfrage gesendet.");
            this.reload();
        } catch (e) {
            console.error(e);
            this.setState({
                    error: JSON.stringify(e),
                    loading: false,
                }
            );
        }
    }

    async sendFriendRequest(userId) {
        try {
            await createFriendRequest(userId);
            this.props.showToast("Gruppenanfrage gesendet.");
            this.reload();
        } catch (e) {
            console.error(e);
            this.setState({
                    error: JSON.stringify(e),
                    loading: false,
                }
            );
        }
    }

    async acceptFriendRequest(userId) {
        try {
            await acceptFriendRequest(userId);
            this.props.showToast("Freundschaftsanfrage angenommen.");
            this.reload();
        } catch (e) {
            console.error(e);
            this.setState({
                    error: JSON.stringify(e),
                    loading: false,
                }
            );
        }
    }

    async removeFriendRequest(userId) {
        try {
            await deleteFriendRequest(userId);
            this.props.showToast("Freundschaftsanfrage gelöscht.");
            this.reload();
        } catch (e) {
            console.error(e);
            this.setState({
                    error: JSON.stringify(e),
                    loading: false,
                }
            );
        }
    }

    async removeFriend(userId) {
        try {
            await deleteFriend(userId);
            this.props.showToast("Freund entfernt");
            this.reload();
        } catch (e) {
            console.error(e);
            this.setState({
                    error: JSON.stringify(e),
                    loading: false,
                }
            );
        }
    }

    onChangeField(fieldName, newValue) {
        this.setState({
            ...this.state,
            [fieldName]: newValue
        });
    }

    async createNewGroup(){
        //create new group
        let newGroup = await createGroup(this.getNewGroupData());
        //handle exceptions so that Code can proceed!!!
        console.log(newGroup)
        //add affiliation and admin status to user
        /*group_id = newGroup.data.id;
        let adminAffiliationData = {};
        adminAffiliationData.admin_status = true;
        await createGroupAffiliation(adminAffiliationData, group_id)
        //Navigation.navigate("SharingTab", "SharingOverview")
        */
        this.props.showToast("Gruppe erstellt");
    }

    async joinGroup(){
        //try {
            const groups = await fetchGroups();
            let group = groups.data.find(g => g.group_name === this.state.join_group_name);
            if (typeof group == 'undefined'){
                this.props.showToast("Ungültiger Gruppenname");
            }
            else if(group.password !== this.state.join_group_password){
                this.props.showToast("Ungültiges Passwort");
            } 
            else{
                let joinGroupData = {};
                joinGroupData.admin_status = false;
                await createGroupAffiliation(joinGroupData, group.id)
                this.props.showToast("Gruppe beigetreten!");
        //    }
        /*} catch (e) {
            console.log(e);
            this.setState({
                error: JSON.stringify(e),
                loading: false
            }); */
        } 
            /*
            let groupCreatorData = {}
            groupCreatorData.group = groupData;
            groupCreatorData.member = groupData.user;
            groupCreatorData.admin_status = true;
            return groupData;
            */
    }


    getNewGroupData(){
        let groupData = {}
        groupData.group_name = this.state.new_group_name;
        groupData.password = this.state.new_group_password;
        return groupData;
    }

    getGroupCreatorData(groupData){
        let groupCreatorData = {}
        groupCreatorData.group = groupData;
        groupCreatorData.member = groupData.user;
        groupCreatorData.admin_status = true;
        return groupData;
    }


    likePost(){

    }



    // -------------------
    // UI Stuff
    // -------------------

    render() {
        if (this.state.error != null) {
            return (<View style={styles.container}>
                <Text>Sorry, ein Fehler ist aufgetreten</Text>
                <Text>{this.state.error}</Text>
            </View>);
        }

        if (!this.props.signedIn) {
            return (
                <View style={[{
                    display: "flex",
                    alignItems: "center",
                    justifyContent: "center",
                    marginHorizontal: 20
                }, styles.container]}>
                    <Paragraph style={{textAlign: "center"}}>
                        Du musst registriert sein um Freunde hinzufügen zu können.
                    </Paragraph>
                    <Button style={styles.button} mode="contained"
                            onPress={() => Navigation.navigate("AccountTab", "SignIn")}>Login</Button>
                    <Paragraph style={{marginVertical: 10}}>oder</Paragraph>
                    <Button mode="contained" style={{marginVertical: 20}} accessibilityLabel="Jetzt registrieren"
                            onPress={() => Navigation.navigate("AccountTab", "Infotrailer")}>
                        Jetzt registieren
                    </Button>
                </View>
            );
        }

        let viewPane;
        switch (this.state.selectedIndex) {
            case REQUEST_INCOMING_VIEW:
                viewPane = this.GroupOverviewScreen();
                break;
            case REQUEST_OUTGOING_VIEW:
                viewPane = this.renderRequestScreen();
                break;
            default:
                viewPane = this.contentScreen();
        }

        let renderSearchField;
        if (this.state.selectedIndex === SEARCH_VIEW) {
          /* renderSearchField = (<View style={styles.searchView}>
                <Searchbar
                    accessibilityLabel="Suchleiste"
                    style={styles.searchField}
                    placeholder="Nur Beiträge von Gruppe:"
                    value={this.state.friendSearch}
                    onChangeText={this.onSearch.bind(this)}
                />
                
            </View>);
            */
        }


        return (
            <View style={styles.container}>
                <ScrollView
                    keyboardShouldPersistTaps="handled" /* when keyboard is open, allow button press */
                    refreshControl={<RefreshControl refreshing={this.state.loading}
                                                    onRefresh={this.reload.bind(this)}/>}>

                    <SegmentedControlTab
                        values={["Überblick", "Gruppen", "Anfragen"]}
                        tabStyle={{backgroundColor: Colors.veryLightGray, borderWidth: 1.4, borderColor: Colors.green, elevation: 5}}
                        activeTabStyle={{backgroundColor: Colors.green}}
                        tabTextStyle={{color: Colors.black, padding: 3}}
                        selectedIndex={this.state.selectedIndex}
                        onTabPress={this.handleIndexChange}
                    />

                    {renderSearchField}
                    {viewPane}
                </ScrollView>
            </View>

        );
    }

    navigateToUser = (user) => {
        Navigation.navigate("FriendsTab", "FriendDetail", {
            friend: user
        });
    };

    /**
     * Renders a FlatList with the given data as renderItems.
     * The buttons will be displayed on each item.
     *
     * @param data Data to be displayed
     * @param createButtonFunction Function which takes a userId as Argument
     * @returns FlatList
     */
    renderFlatList(data, createButtonFunction) {
        console.log(data)
        return (
            <FlatList
                keyboardShouldPersistTaps="handled"
                data={data}
                refreshing={this.state.loading}
                onRefreash={this.reload.bind(this)}
                renderItem={({item}) => (
                    <TouchableRipple accessibilityLabel={"Benutzer - " + item.username}
                                     style={styles.itemTouchableContainer}
                                     onPress={() => this.navigateToUser(item)}>
                        <View style={styles.itemContainer}>
                            <Text style={styles.username}>{item.username}</Text>
                            <View style={styles.buttonContainer}>
                                {createButtonFunction(item.id)}
                            </View>
                        </View>
                    </TouchableRipple>
                )}
                keyExtractor={item => item.id + ""}
            />
        );
    }

    renderGroupFlatList(data, createButtonFunction) {
        return (
            <FlatList
                keyboardShouldPersistTaps="handled"
                data={data}
                refreshing={this.state.loading}
                onRefreash={this.reload.bind(this)}
                renderItem={({item}) => (
                    <TouchableRipple accessibilityLabel={"Benutzer - "}
                                     style={styles.itemTouchableContainer}
                                     onPress={() => this.navigateToGroup(item)}>
                        <View style={styles.itemContainer}>
                            <Text style={styles.username}>{item}</Text>
                            <View style={styles.buttonContainer}>
                                {createButtonFunction(item.id)}
                            </View>
                        </View>
                    </TouchableRipple>
                )}
                keyExtractor={item => item.id + ""}
            />
        );
    }

    renderContentFlatList(data, createButtonFunction) {
        return (
            <FlatList
                keyboardShouldPersistTaps="handled"
                data={data}
                refreshing={this.state.loading}
                onRefreash={this.reload.bind(this)}
                renderItem={({item}) => (
                    <TouchableRipple accessibilityLabel={"Geteilte Inhalte"}
                                     style={styles.itemTouchableContainer}>
                        <View style={styles.itemContainer}>
                            <Text style={styles.username}>{item.contentText}</Text>
                            <View style={styles.buttonContainer}>
                                {createButtonFunction(item.id)}
                            </View>
                        </View>
                    </TouchableRipple>
                )}
                keyExtractor={item => item.id + ""}
            />
        );
    }

    createGroupForms() {
        return (
            <KeyboardAvoidingView style={styles.container}>
                    <ScrollView keyboardShouldPersistTaps="handled" style={styles.container}>
                        <ErrorBox errors={this.state.errors}></ErrorBox>
                        <TextInput
                            style={styles.formControl}
                            label='Benutzername'
                            autoCapitalize="none"
                            accessibilityLabel="Benutzername"
                            value={this.state.username}
                            onChangeText={val => this.onChangeField("username", val)}
                        />

                        <TextInput
                            label='Passwort'
                            style={styles.formControl}
                            autoCapitalize="none"
                            accessibilityLabel="Passwort"
                            secureTextEntry={true}
                            value={this.state.password}
                            onChangeText={val => this.onChangeField("password", val)}
                        />
                        <Button style={styles.formControl} mode="contained" accessibilityLabel="Login">Login</Button>
                        <Paragraph/>
                        <Paragraph style={{textAlign: "center"}}>oder</Paragraph>

                        <Button mode="contained" style={{marginVertical: 20}} accessibilityLabel="Registrieren"
                                onPress={() => Navigation.navigate("SignUp")}>
                            Registieren
                        </Button>
                    </ScrollView>
                </KeyboardAvoidingView>
        );
    }

    // -------------------
    // Buttons
    // -------------------
    addFriendButton = (userId) => (
        <IconButton small
                    icon="account-multiple-plus"
                    color={Colors.lightGreen}
                    accessibilityLabel={"Freund hinzufügen - " + userId}
                    onPress={() => this.sendFriendRequest(userId)}
                    key={userId + "addFriend"}
        />
    );

    confirmFriendRequestButton = (userId) => (
        <IconButton small
                    icon="account-check"
                    color={Colors.lightGreen}
                    accessibilityLabel={"Freundschaftsanfrage bestätigen - " + userId}
                    onPress={() => this.acceptFriendRequest(userId)}
                    key={userId + "confirmRequest"}
        />
    );

    confirmFriendRequestButton = (userId) => (
        <IconButton small
                    icon="account-check"
                    color={Colors.lightGreen}
                    accessibilityLabel={"Freundschaftsanfrage bestätigen - " + userId}
                    onPress={() => this.acceptFriendRequest(userId)}
                    key={userId + "confirmRequest"}
        />
    );

    deleteFriendRequestButton = (userId) => (
        <IconButton small
                    icon="account-remove"
                    color={Colors.red}
                    accessibilityLabel={"Freundschaftsanfrage löschen - " + userId}
                    onPress={() => this.removeFriendRequest(userId)}
                    key={userId + "deleteRequest"}
        />
    );

    deleteFriendshipButton = (userId) => (
        <IconButton small
                    icon="account-off"
                    color={Colors.red}
                    accessibilityLabel={"Freund entfernen - " + userId}
                    onPress={() => this.removeFriend(userId)}
                    key={userId + "removeFriend"}
        />
    );

    addLikeButton = (contentId) => (
        <IconButton small
                    icon="account-off"
                    color={Colors.green}
                    accessibilityLabel={"Content liken - " + contentId}
                    onPress={() => this.likePost(contentId)}
                    key={contentId + "likeContent"}
        />
    );

    buttonsForStrangerListItem = (userId) => {
        const arr = [];
        const type = this.state.userTypeMap.get(userId);

        if (type === UT_FRIEND_REQUEST_SENT) {
            arr.push(this.deleteFriendRequestButton);
        } else if (type === UT_FRIEND_REQUEST_INCOMING) {
            arr.push(this.confirmFriendRequestButton);
            arr.push(this.deleteFriendRequestButton);
        } else {
            arr.push(this.addFriendButton);
        }

        return (
            arr.map((buttonFn) => {
                return buttonFn(userId);
            }));
    };

    // -------------------
    // Screens
    // -------------------

    contentScreen() {
        const sharedContentData = this.state.content;
        const likeButton = this.addLikeButton


        let contentList;
        if (sharedContentData.length > 0) {
            contentList = (<View>
                <Headline style={styles.heading}>Shared Content</Headline>
                {this.renderContentFlatList(sharedContentData, likeButton)}
            </View>);
        }
        

        return (
            <View>
                {contentList}
            </View>
        );
    }

    GroupOverviewScreen() {
        const incomingRequestData = this.state.requestIncoming;
        

        const arr = [this.confirmFriendRequestButton, this.deleteFriendRequestButton];
        const incomingRequestButtons = (userId) => {
            return arr.map(buttonFn => {
                return buttonFn(userId);
            });
        };
        
        const sharedContentData = this.state.groups;
        const likeButton = this.addLikeButton

        let contentList;
        if (sharedContentData.length > 0) {
            contentList = (<View>
                {this.renderGroupFlatList(sharedContentData, likeButton)}
            </View>);
        }

        return (
            <View>
                <Headline style={styles.heading}>Gruppe erstellen</Headline>
                <TextInput
                    placeholder="Gruppenname"
                    keyboardType="numeric"
                    value={this.state.new_group_name}
                    style={styles.textinput}
                    onChangeText={val => this.onChangeField("new_group_name", val)}
                />
                <TextInput
                    placeholder="Passwort"
                    keyboardType="numeric"
                    value={this.state.new_group_password}
                    style={styles.textinput}
                    onChangeText={val => this.onChangeField("new_group_password", val)}
                />
                <Button 
                    style={styles.button}
                    onPress={() => this.createNewGroup()}>
                    Erstellen
                </Button>


                <Headline style={styles.heading}>Deine Gruppen</Headline>
                {contentList}
            </View>
        );
    }

    renderRequestScreen() {
        return (
            <View>
                <Headline style={styles.heading}>Gruppe beitreten</Headline>
                <TextInput
                    placeholder="Gruppenname"
                    keyboardType="numeric"
                    style={styles.textinput}
                    onChangeText={val => this.onChangeField("join_group_name", val)}
                />
      
                <TextInput
                    placeholder="Passwort"
                    keyboardType="numeric"
                    style={styles.textinput}
                    onChangeText={val => this.onChangeField("join_group_password", val)}
                />
      <Button style={styles.button} onPress={() => this.joinGroup()}>Beitreten</Button>
      <Divider />

                <Headline style={styles.heading}>Erhaltene Anfragen</Headline>

            </View>
        );
    }

}

const {width, height} = Dimensions.get("window");
const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 10,
    },
    searchField: {
        marginTop: 20,
        marginLeft: 5,
        maxWidth: width,
        color: "lightgrey",
    },
    graySearchField: {
        marginTop: 20,
        marginLeft: 5,
        maxWidth: width,
        color: "green",
    },
    button: {
        marginVertical: 8,
        minWidth: "50%",
    },
    itemTouchableContainer: {
        marginTop: 8,
        marginBottom: 8,
        paddingTop: 10,
        paddingBottom: 10,
        paddingLeft: 10,
        borderBottomColor: "lightgrey",
        borderTopColor: "lightgrey",
        borderBottomWidth: 1,
        borderTopWidth: 1,
        height: 60,
        display: "flex",
        justifyContent: "center"
    },
    itemContainer: {
        display: "flex",
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "center",
    },
    buttonContainer: {
        display: "flex",
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "center",
    },
    heading: {
        marginTop: 30,
        marginBottom: 10,
        textAlign: "center",
    },
    username: {
        fontSize: 18,
    },
    searchView: {
        display: "flex",
        flexDirection: "row",
        alignItems: "flex-end",
        justifyContent: "flex-end"
    },
    textinput: {
        color: "white",
        marginLeft: 5,
        maxWidth: width
    },
    scannerToggler: {}
});

const mapStateToProps = state => {
    return {
        signedIn: !state.account.account.pseudonymous,
        username: state.account.account.username
    };
};

const mapDispatchToProps = dispatch => bindActionCreators({
    showToast,
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(SharingOverviewScreen);