import React from "react";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import {StyleSheet, ScrollView} from 'react-native';
import {Text, Paragraph, FAB, ProgressBar} from "react-native-paper";
import Navigation from "../../../services/Navigation";
import {Colors} from "../../../styles";

class ViewFinalReportIntroScreen extends React.Component {

    constructor(props) {
        super(props);
    }

    navigateToFinalReportButton() {
        return (<FAB
            style={styles.button}
            mode="contained"
            label="Wie ist es dir mit deinem Self-Commitment ergangen?"
            accessibilityLabel="Wie ist es dir mit deinem Self-Commitment ergangen?"
            onPress={() => Navigation.navigate("DiaryTab", "ViewFinalReport", {diary: this.props?.route?.params.diary})}/>);
    }

    computeDays() {
        let days = this.props?.route?.params.diary.challenge.duration;
        return days;
    }

    checkIfSelfCommitmentSuccessful() {
        const dailyMonitorings = this.props?.route?.params.diary.daily_monitorings;
        const challengeDuration = this.props?.route?.params.diary.challenge.duration;
        const successfulMessage = "Du hast Dein Self-Commitment eingelöst! Super, dass Du durchgehalten hast! Deine Erfahrungen unterstützen Dich und andere in ihren Self-Commitments für eine nachhaltigere und gerechtere Welt!";
        const failedMessage = "Du hast Dich " + challengeDuration + " Tage Deinem Self-Commitment gestellt! Super, dass Du durchgehalten hast! Es gab Faktoren, die Deinem Self-Commitment in die Quere kamen. Deine Erfahrungen sind sehr wertvoll – sie unterstützen Dich und andere in ihren Self-Commitments für eine nachhaltigere und gerechtere Welt.";
        let successfulCounter = 0;

        dailyMonitorings.forEach(dailyMonitoring => {
            if(dailyMonitoring.done) successfulCounter++;
        })

        if(successfulCounter == challengeDuration) {
            return successfulMessage;
        } else {
            return failedMessage;
        }
    }

    render() {
        const selfCommitment = this.props?.route?.params.diary.selfCommitment;
        const days = this.computeDays();

        return (
            <ScrollView style={styles.contentWrapper}>
                <Text style={styles.progressBarText}>1 von 4</Text>
                <ProgressBar progress={0.25} style={styles.progressBar}/>
                <Paragraph>Du hast Dich {days} Tage mit Deinem Self-Commitment befasst:</Paragraph>
                <Paragraph style={styles.selfCommitmentParagraph}>"{selfCommitment}"</Paragraph>
                <Paragraph>{this.checkIfSelfCommitmentSuccessful()}</Paragraph>
                <Paragraph/>
                <Paragraph/>
                {this.navigateToFinalReportButton()}
            </ScrollView>);
    }
}

const styles = StyleSheet.create({
    button: {
        marginHorizontal: 10,
        marginVertical: 10,
        backgroundColor: Colors.green,
    },
    contentWrapper: {
        paddingHorizontal: 10,
        paddingVertical: 10
    },
    progressBarText: {
        textAlign: 'center'
    },
    progressBar: {
        marginBottom: 20
    },
    selfCommitmentParagraph: {
        textAlign: "center",
        paddingTop: 20,
        paddingBottom: 20,
        fontWeight: "bold"
    }
});

const mapStateToProps = state => {
    return {};
};

const mapDispatchToProps = dispatch => bindActionCreators({}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(ViewFinalReportIntroScreen);