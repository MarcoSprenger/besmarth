import React from "react";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import {StyleSheet, View, ScrollView, Linking} from 'react-native';
import Slider from '@react-native-community/slider';
import {Text, Paragraph, FAB, ProgressBar, Surface, Divider} from "react-native-paper";
import Navigation from "../../../services/Navigation";
import {Colors} from "../../../styles";

class FinalReportSurveyScreen extends React.Component {

    diaryData = {};

    constructor(props) {
        super(props);
        this.state = {
        };
        this.diaryData = this.props?.route?.params.diary;
    }

    goToSurveyAndDiaryOverview() {
        Linking.openURL('https://survey.fhnw.ch/uc/BNE-App')
        Navigation.navigate("DiaryTab", "ExperienceOverview")
    }


    navigateToFinalReportSurveyButton() {
        return (<FAB
            style={styles.button}
            mode="contained"
            label="Zum Fragebogen"
            accessibilityLabel="Weiter"
            onPress={()=>{ this.goToSurveyAndDiaryOverview()}}/>);
    }

    handler = (propName) => (value) => {
        this.setState({
            ...this.state,
            [propName]: value
        });
    }



    render() {
        return (
            <ScrollView style={styles.contentWrapper}>
                <Text style={{textAlign: 'center'}}>4 von 4</Text>
                <ProgressBar progress={1.0} style={{marginBottom: 20}}/>
                <Paragraph/>
                <Divider/>
                <Text> Wir wollen die App verbessern und weiterentwickeln, bitte sage uns über den Fragebogen, welche Gedanken du dazu hast. </Text>
                <Paragraph/>
                {this.navigateToFinalReportSurveyButton()}
                <Paragraph></Paragraph>
            </ScrollView>);
    }
}

const styles = StyleSheet.create({
    row: {
        flexDirection: "row",
        justifyContent: "space-between",
    },
    button: {
        marginHorizontal: 10,
        marginVertical: 10,
        backgroundColor: Colors.green,
    },
    contentWrapper: {
        paddingHorizontal: 10,
        paddingVertical: 10
    },
    card: {
        margin: 10,
        borderRadius: 15,
        display: "flex",
        flexDirection: "column",
        alignItems: "stretch",
        justifyContent: "space-between",
        elevation: 4,
        padding: 10,
    },
    paragraph: {
        paddingBottom: 10
    }
});

const mapStateToProps = state => {
    return {};
};

const mapDispatchToProps = dispatch => bindActionCreators({}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(FinalReportSurveyScreen);