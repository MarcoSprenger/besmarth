import React from "react";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import {StyleSheet, View, ScrollView} from 'react-native';
import Slider from '@react-native-community/slider';
import {Text, Paragraph, FAB, ProgressBar, Surface, Divider} from "react-native-paper";
import Navigation from "../../../services/Navigation";
import {Colors} from "../../../styles";

class ViewFinalReportScreen extends React.Component {

    diaryData = {};

    constructor(props) {
        super(props);
        this.state = {};
        this.diaryData = this.props?.route?.params.diary;
    }

    getHypothesisStart() {
        let hypothesisStart;
        hypothesisStart = this.diaryData.hypothesises.find(hypo => hypo.startHypothesis === true && hypo.endHypothesis === false);
        return hypothesisStart;
    }

    getHypothesisEnd() {
        let hypothesisEnd;
        hypothesisEnd = this.diaryData.hypothesises.find(hypo => hypo.startHypothesis === false && hypo.endHypothesis === true);
        return hypothesisEnd;
    }

    sliderOptions(negative, positive) {
        return (
            <View style={{...styles.row}}>
                <Text>{negative}</Text>
                <Text>{positive}</Text>
            </View>);
    }

    navigateToFinalReportFactorsButton() {
        return (<FAB
            style={styles.button}
            mode="contained"
            label="Weiter"
            accessibilityLabel="Weiter"
            onPress={() => Navigation.navigate("DiaryTab", "ViewFinalReportFactors", {diary: this.diaryData})}/>);
    }

    hypothesisSliders() {
        const startHypothesises = this.getHypothesisStart();
        const endHypothesises = this.getHypothesisEnd();
        return (<View>
            <Surface style={styles.card}>
                <Paragraph style={styles.paragraph}>Die Einlösung dieses Self-Commitments war für mich</Paragraph>
                <Slider
                    thumbTintColor={Colors.green}
                    maximumValue={1}
                    minimumValue={0}
                    disabled={true}
                    value={parseFloat(startHypothesises.difficulty)}
                />
                {this.sliderOptions("sehr schwierig", "sehr leicht")}
                <Slider
                    thumbTintColor={Colors.green}
                    maximumValue={1}
                    minimumValue={0}
                    disabled={true}
                    value={parseFloat(endHypothesises.difficulty)}
                />
            </Surface>

            <Surface style={styles.card}>
                <Paragraph style={styles.paragraph}>Dieses Self-Commitment hat mich</Paragraph>
                <Slider
                    thumbTintColor={Colors.green}
                    maximumValue={1}
                    minimumValue={0}
                    disabled={true}
                    value={parseFloat(startHypothesises.limitation)}
                />
                {this.sliderOptions("stark eingeschränkt", "befreit")}
                <Slider
                    thumbTintColor={Colors.green}
                    maximumValue={1}
                    minimumValue={0}
                    disabled={true}
                    value={parseFloat(endHypothesises.limitation)}
                />
            </Surface>

            <Surface style={styles.card}>
                <Paragraph style={styles.paragraph}>Mein Umfeld hat auf mein Self-Commitment</Paragraph>
                <Slider
                    thumbTintColor={Colors.green}
                    maximumValue={1}
                    minimumValue={0}
                    disabled={true}
                    value={parseFloat(startHypothesises.environment)}
                />
                {this.sliderOptions("negativ reagiert", "positiv reagiert")}
                <Slider
                    thumbTintColor={Colors.green}
                    maximumValue={1}
                    minimumValue={0}
                    disabled={true}
                    value={parseFloat(endHypothesises.environment)}
                />
            </Surface>

            <Surface style={styles.card}>
                <Paragraph style={styles.paragraph}>
                    Verglichen mit meinem bisherigen Engagement, leiste ich mit diesem Self-Commiment einen</Paragraph>
                <Slider
                    thumbTintColor={Colors.green}
                    maximumValue={1}
                    minimumValue={0}
                    disabled={true}
                    value={parseFloat(startHypothesises.contribution)}
                />
                {this.sliderOptions("kleinen Beitrag", "grossen Beitrag")}
                <Slider
                    thumbTintColor={Colors.green}
                    maximumValue={1}
                    minimumValue={0}
                    disabled={true}
                    value={parseFloat(endHypothesises.contribution)}
                />
            </Surface>

            <Surface style={styles.card}>
                <Paragraph style={styles.paragraph}>Das Self-Commitment ermöglicht mir vor allem</Paragraph>
                <Slider
                    thumbTintColor={Colors.green}
                    maximumValue={1}
                    minimumValue={0}
                    disabled={true}
                    value={parseFloat(startHypothesises.burden)}
                />
                {this.sliderOptions("Verantwortung\nzu übernehmen", "Erwartungen\nnachzukommen")}
                <Slider
                    thumbTintColor={Colors.green}
                    maximumValue={1}
                    minimumValue={0}
                    disabled={true}
                    value={parseFloat(endHypothesises.burden)}
                />
            </Surface>
        </View>);
    }

    render() {
        const days = this.diaryData.challenge.duration;
        return (
            <ScrollView style={styles.contentWrapper}>
                <Text style={{textAlign: 'center'}}>2 von 4</Text>
                <ProgressBar progress={0.5} style={{marginBottom: 20}}/>
                <Paragraph/>
                <Paragraph>So hast du dein Self-Commitment vor {days} Tagen beurteilt (grauer Balken), wie beurteilst du es jetzt?</Paragraph>
                <Paragraph/>
                <Divider/>
                {this.hypothesisSliders()}
                {this.navigateToFinalReportFactorsButton()}
                <Paragraph></Paragraph>
            </ScrollView>);
    }
}

const styles = StyleSheet.create({
    row: {
        flexDirection: "row",
        justifyContent: "space-between",
    },
    button: {
        marginHorizontal: 10,
        marginVertical: 10,
        backgroundColor: Colors.green,
    },
    contentWrapper: {
        paddingHorizontal: 10,
        paddingVertical: 10
    },
    card: {
        margin: 10,
        borderRadius: 15,
        display: "flex",
        flexDirection: "column",
        alignItems: "stretch",
        justifyContent: "space-between",
        elevation: 4,
        padding: 10,
    },
    paragraph: {
        paddingBottom: 10
    }
});

const mapStateToProps = state => {
    return {};
};

const mapDispatchToProps = dispatch => bindActionCreators({}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(ViewFinalReportScreen);