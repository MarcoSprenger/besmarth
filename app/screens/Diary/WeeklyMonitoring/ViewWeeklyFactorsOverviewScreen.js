import React from "react";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import {StyleSheet, ScrollView, SectionList} from 'react-native';
import {Text, Paragraph, Surface, FAB, ProgressBar, Divider} from "react-native-paper";
import Navigation from "../../../services/Navigation";
import {Colors} from "../../../styles";

class ViewWeeklyFactorsOverviewScreen extends React.Component {

    // Diary data to display and pass to next screen
    data = {};

    constructor(props) {
        super(props);
        this.state = {};
        this.data = this.props?.route?.params.data;
    }

    navigateToWeeklyActionsButton() {
        return (<FAB
            style={styles.button}
            mode="contained"
            label="Weiter"
            accessibilityLabel="Weiter"
            onPress={() => Navigation.navigate("DiaryTab", "ViewWeeklyActions", {data: this.data})}/>);
    }

    getPostList() {
        const postList = [
            {
                title: "Erleichternd",
                data: []
            },
            {
                title: "Erschwerend",
                data: []
            }
        ];
        let postsData = this.data.diary.posts;
        postsData = postsData.filter(post => post.week === this.data.week);
        postsData.forEach(post => {
            if (post.positive) {
                let postForList = {
                    text: "❓: " + post.text
                };
                postList[0].data.push(postForList);
            } else {
                let postForList = {
                    text: "❗: " + post.text
                };
                postList[1].data.push(postForList);
            }
        })
        return postList;
    }

    listPositiveAndNegativePosts() {
        return (
            <ScrollView>
                <Surface style={styles.card}>
                    <SectionList
                        ItemSeparatorComponent={this.ItemSeparator}
                        sections={this.getPostList()}
                        renderItem={({item}) => <Text style={styles.item}>{item.text}</Text>}
                        keyExtractor={(item, index) => index}
                    />
                </Surface>
            </ScrollView>
        )
    }

    getFactorList() {
        const factorList = [
            {
                title: "Erleichternd",
                data: []
            },
            {
                title: "Erschwerend",
                data: []
            }
        ];
        let factorsData = this.data.diary.factors;
        factorsData = factorsData.filter(factor => factor.week === this.data.week && !factor.finalFactor);
        factorsData.forEach(factor => {
            if (factor.positive) {
                let factorForList = {
                    text: "❓: " + factor.text
                };
                factorList[0].data.push(factorForList);
            } else {
                let factorForList = {
                    text: "❗: " + factor.text
                };
                factorList[1].data.push(factorForList);
            }
        })
        return factorList;
    }

    listPositiveAndNegativeFactors() {
        return (
            <ScrollView>
                <Surface style={styles.card}>
                    <SectionList
                        ItemSeparatorComponent={this.ItemSeparator}
                        sections={this.getFactorList()}
                        renderItem={({item}) => <Text style={styles.item}>{item.text}</Text>}
                        keyExtractor={(item, index) => index}
                    />
                </Surface>
            </ScrollView>
        )
    }

    render() {
        const selfCommitment = this.data.diary.selfCommitment;
        return (
            <ScrollView style={styles.contentWrapper}>
                <Text style={{textAlign: 'center'}}>2 von 3</Text>
                <ProgressBar progress={0.66} style={{marginBottom: 20}}/>
                <Paragraph>Tagebucheinträge Self-Commitment "{selfCommitment}":</Paragraph>
                {this.listPositiveAndNegativePosts()}
                <Paragraph/>
                <Divider/>
                <Paragraph/>
                <Paragraph>Welche Erfahrungen, welche Fragen haben dich diese Woche im Zusammenhang mit deinem Self-Commitment umgetrieben? Und welche Schlüsse kannst du daraus ziehen, die du an dieser Stelle auch andern weitergeben möchtest?*         </Paragraph>
                <Paragraph/>
                {this.listPositiveAndNegativeFactors()}
                <Paragraph/>
                {this.navigateToWeeklyActionsButton()}
                <Paragraph/>
            </ScrollView>);
    }
}

const styles = StyleSheet.create({
    item: {
        fontSize: 14,
        borderWidth: 0.2,
        borderColor: "#f2f2f2",
        borderRadius: 15,
        padding: 3,
        marginVertical: 2,
        backgroundColor: "#f2f2f2"
    },
    button: {
        marginHorizontal: 10,
        marginVertical: 10,
        backgroundColor: Colors.green,
    },
    contentWrapper: {
        paddingHorizontal: 10,
        paddingVertical: 10
    },
    card: {
        margin: 10,
        borderRadius: 15,
        display: "flex",
        flexDirection: "column",
        alignItems: "stretch",
        justifyContent: "space-between",
        elevation: 4,
        padding: 5,
    },
});

const mapStateToProps = state => {
    return {};
};

const mapDispatchToProps = dispatch => bindActionCreators({}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(ViewWeeklyFactorsOverviewScreen);