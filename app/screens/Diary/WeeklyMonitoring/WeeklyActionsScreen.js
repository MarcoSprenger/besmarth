import React from "react";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import {StyleSheet, View, ScrollView, Alert} from 'react-native';
import {Text, Paragraph, TextInput, FAB, ProgressBar, Divider} from "react-native-paper";
import Navigation from "../../../services/Navigation";
import {Colors} from "../../../styles";
import {showToast} from "../../../services/Toast";
import {createFactor, createWeeklyMonitoring} from "../../../services/DiaryService";
import {createSharedContent} from "../../../services/SharedContentService";


class WeeklyActionsScreen extends React.Component {

    data = {};

    constructor(props) {
        super(props);
        this.state = {
            loading: true,
            ownActions: "",
            socialActions: "",
            politicalActions: "",
            productManufacturerActions: "",
        };
        this.data = this.props?.route?.params.data;
    }

    navigateToExperienceOverviewButton() {
        return (<FAB
            style={styles.button}
            mode="contained"
            label="Abschliessen"
            accessibilityLabel="Abschliessen"
            onPress={() => this.weeklyFinished()}/>);
    }

    async weeklyFinished() {
        let factors = this.data.factors;
        let diaryId = this.data.diary.id;
        let weeklyMonitoringData = this.getWeeklyMonitoringData();
        await factors.forEach(factor => {
            createFactor(diaryId, factor);
            createSharedContent(this.prepareDataForFactorsSharedContentCreation(factor));
        })
        await createWeeklyMonitoring(diaryId, weeklyMonitoringData);

        await createSharedContent(this.prepareDataForActionsSharedContentCreation("Eigene Massnahmen: ", weeklyMonitoringData.self_action));
        await createSharedContent(this.prepareDataForActionsSharedContentCreation("Massnahmen für das soziale Umfeld: ", weeklyMonitoringData.socialEnvironment_action));
        await createSharedContent(this.prepareDataForActionsSharedContentCreation("Massnahmen auf politischer Ebene: ", weeklyMonitoringData.political_action));
        await createSharedContent(this.prepareDataForActionsSharedContentCreation("Massnahmen auf Ebene der Produkthersteller: ", weeklyMonitoringData.producer_action));


        Navigation.navigate("DiaryTab", "ExperienceOverview")
        this.props.showToast("Wöchentlicher Statusbericht abgeschlossen!")
        if (this.getWeeklyMonitoringData().week === 4) {
            this.props.showToast("Rückblick ist nun verfügbar!")
        }
    }



    prepareDataForFactorsSharedContentCreation = (factorElement) => {
        let data = {};
        data.contentText = "Wöchentlicher Faktor: " + factorElement.text;
        data.contentType = "FACTORS";
        return data;
    }


    prepareDataForActionsSharedContentCreation = (actionType, actionText) => {
        let data = {};
        data.contentText = actionType + actionText;
        data.contentType = "ACTIONS";
        return data;
    }



    


    getWeeklyMonitoringData() {
        let weeklyMonitoring = {
            week: this.data.week,
            self_action: this.state.ownActions,
            socialEnvironment_action: this.state.socialActions,
            political_action: this.state.politicalActions,
            producer_action: this.state.productManufacturerActions
        };
        return weeklyMonitoring;
    }

    onChangeField(fieldName, newValue) {
        this.setState({
            ...this.state,
            [fieldName]: newValue
        });
    }

    actionsWithTextField() {
        return (
            <View>
                <TextInput
                    multiline={true}
                    label="… bei dir selbst"
                    value={this.state.ownActions}
                    accessibilityLabel="… bei dir selbst"
                    onChangeText={val => this.onChangeField("ownActions", val)}
                />
                <Paragraph/>
                <TextInput
                    multiline={true}
                    label="… in deinem sozialen Umfeld"
                    value={this.state.socialActions}
                    accessibilityLabel="… in deinem sozialen Umfeld"
                    onChangeText={val => this.onChangeField("socialActions", val)}
                />
                <Paragraph/>
                <TextInput
                    multiline={true}
                    label="… auf politischer Ebene"
                    value={this.state.politicalActions}
                    accessibilityLabel="… auf politischer Ebene"
                    onChangeText={val => this.onChangeField("politicalActions", val)}
                />
                <Paragraph/>
                <TextInput
                    multiline={true}
                    label="… auf der Ebene von Produktherstellern"
                    value={this.state.productManufacturerActions}
                    accessibilityLabel="… auf der Ebene von Produktherstellern"
                    onChangeText={val => this.onChangeField("productManufacturerActions", val)}
                />
            </View>
        )
    }

    render() {

        return (
            <ScrollView style={styles.contentWrapper}>
                <Text style={{textAlign: 'center'}}>3 von 3</Text>
                <ProgressBar progress={1.0} style={{marginBottom: 20}}/>
                <Paragraph>Was müsste aufgrund deiner gemachten Erfahrungen konkret angestossen werden?</Paragraph>
                <Paragraph/>
                <Divider/>
                <Paragraph>Massnahmen:</Paragraph>
                {this.actionsWithTextField()}
                <Paragraph>* Diese Eingaben werden, falls du einer Gruppe zugehörst, mit dieser geteilt. Du kannst die Eingaben der andern Gruppenmitglieder über das Menü Sharing einsehen und sie wissen lassen, wenn du ihre Beiträge gut findest.</Paragraph>
                <Paragraph/>
                {this.navigateToExperienceOverviewButton()}
                <Paragraph/>
            </ScrollView>);
    }
}

const styles = StyleSheet.create({
    button: {
        marginHorizontal: 10,
        marginVertical: 10,
        backgroundColor: Colors.green,
    },
    contentWrapper: {
        paddingHorizontal: 10,
        paddingVertical: 10
    },
});

const mapStateToProps = state => {
    return {};
};

const mapDispatchToProps = dispatch => bindActionCreators({
    showToast
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(WeeklyActionsScreen);