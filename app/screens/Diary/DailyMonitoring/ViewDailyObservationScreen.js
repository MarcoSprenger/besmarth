import React from "react";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import {ScrollView, StyleSheet, View} from 'react-native';
import Slider from '@react-native-community/slider';
import {Text, Paragraph, Switch, FAB, ProgressBar, Divider, Surface} from "react-native-paper";
import Navigation from "../../../services/Navigation";
import {Colors} from "../../../styles";
import { Entypo } from '@expo/vector-icons';

class ViewDailyObservationScreen extends React.Component {

    constructor(props) {
        super(props);
        this.state = {};
    }

    getCurrentDailyMonitoring() {
        let today = new Date();
        let dd = String(today.getDate()).padStart(2, '0');
        let mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
        let yyyy = today.getFullYear();
        today = yyyy + "-" + mm + "-" + dd;
        let currentDailyMonitoring = this.props?.route?.params.diary.daily_monitorings.find(dailyMonitoring => dailyMonitoring.timestamp === today);
        return currentDailyMonitoring;
    }

    sliderOptions() {
        return (
            <View style={{...styles.row}}>
                <Entypo name="emoji-sad" size={24} />
                <Entypo name="emoji-neutral" size={24} />
                <Entypo name="emoji-happy" size={24} />
            </View>);
    }

    challengeCompletedView(selfCommitment) {
        return (
            <View style={{alignItems: "center"}}>
                <Surface style={styles.card}>
                    <Paragraph>Hast du dein Self-Commitment "{selfCommitment}" heute eingelöst?</Paragraph>
                    <View style={{...styles.row}}>
                        <Text>Nein</Text>
                        <Switch value={this.getCurrentDailyMonitoring().done}
                                trackColor={{true: 'green', false: 'red'}}
                                accessibilityLabel="Self-Commitment eingelöst?"
                                disabled={true}/>
                        <Text>Ja</Text>
                    </View>
                </Surface>
            </View>
        );
    }

    navigateToDailyReflectionButton() {
        return (<FAB
            style={styles.button}
            mode="contained"
            label="Weiter"
            accessibilityLabel="Weiter"
            onPress={() => Navigation.navigate("DiaryTab", "ViewDailyReflection", {
                diary: this.props?.route?.params.diary
            })}/>);
    }

    render() {
        const dailyObservationText = "Diese täglichen Reflexionsfragen ermöglichen dir, Deine Erfahrungen mit dem Self-Commitment zu " +
            "sammeln und nutzbar zu machen, um Möglichkeiten für individuelle, gesellschaftliche und politische " +
            "Veränderungen hin zu einer nachhaltigeren und gerechteren Welt zu finden."
        const selfCommitment = this.props?.route?.params.diary.selfCommitment;

        return (
            <ScrollView style={styles.contentWrapper}>
                <Text style={{textAlign: 'center'}}>1 von 2</Text>
                <ProgressBar progress={0.5} style={{marginBottom: 20}}/>
                <Paragraph>Hallo! Wie geht es dir heute?</Paragraph>
                <Paragraph>{dailyObservationText}</Paragraph>
                <Divider/>
                <Surface style={styles.card}>
                    <Paragraph>Heute fühlte ich mich</Paragraph>
                    {this.sliderOptions()}
                    <Slider
                        style={{padding: 20}}
                        maximumValue={1}
                        minimumValue={0}
                        step={0.1}
                        value={parseFloat(this.getCurrentDailyMonitoring().wellbeing)}
                        disabled={true}
                    />
                    <Paragraph>als ohne das Self-Commitment</Paragraph>
                </Surface>
                {this.challengeCompletedView(selfCommitment)}
                {this.navigateToDailyReflectionButton()}
            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    row: {
        display: "flex",
        flexDirection: "row",
        justifyContent: "space-between",
        paddingTop: 15,
    },
    button: {
        marginHorizontal: 10,
        marginVertical: 10,
        backgroundColor: Colors.green,
    },
    contentWrapper: {
        paddingHorizontal: 10,
        paddingVertical: 10
    },
    card: {
        margin: 10,
        borderRadius: 15,
        display: "flex",
        flexDirection: "column",
        alignItems: "stretch",
        justifyContent: "space-between",
        elevation: 4,
        padding: 10,
    },
});

const mapStateToProps = state => {
    return {};
};

const mapDispatchToProps = dispatch => bindActionCreators({}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(ViewDailyObservationScreen);