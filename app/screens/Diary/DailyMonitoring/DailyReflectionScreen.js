import React from "react";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import {Dimensions, ScrollView, SectionList, StyleSheet, View, Alert} from 'react-native';
import {
    Text,
    Paragraph,
    Button,
    FAB,
    TextInput,
    ProgressBar,
    Surface,
    Divider
} from "react-native-paper";
import Navigation from "../../../services/Navigation";
import {Typography} from "../../../styles";
import {Ionicons} from "@expo/vector-icons";
import {Colors} from "../../../styles";
import {showToast} from "../../../services/Toast";
import {createPost, createDailyMonitoring} from "../../../services/DiaryService";
import {fetchDailyHelpTexts} from "../../../services/DailyHelpTextService";


class DailyReflectionScreen extends React.Component {

    constructor(props) {
        super(props);
        this.forceUpdateHandler = this.forceUpdateHandler.bind(this);
        this.state = {
            loading: true,
            post: "",
            postList: [
                {
                    title: "Erleichternd",
                    data: []
                },
                {
                    title: "Erschwerend",
                    data: []
                }
            ],
            minimalPostList: [],
            week: 0,
            help_text: "",
            help_button_text: "Ich brauche eine Reflexionsfrage"
        };
    }

    //reload Screen
    forceUpdateHandler(){
        this.forceUpdate();
      };

    returnCurrentWeek = (dailyMonitorings) => {
        if(dailyMonitorings.length < 7){
            return 1
        }
        if(dailyMonitorings.length < 14){
            return 2
        }
        if(dailyMonitorings.length < 21){
            return 3
        }    
        if(dailyMonitorings.length < 28){
            return 4
        }
    }

    navigateToExperienceOverviewButton() {
        return (<FAB
            style={styles.button}
            mode="contained"
            label="Tagebucheintrag für heute beenden"
            accessibilityLabel="Tagebucheintrag für heute beenden"
            onPress={() => this.dailyFinished()}/>);
    }

    async dailyFinished() {
        if (this.state.post !== "") {
            Alert.alert("Hinweis", "Zuerst den noch offenen Post anhand der ! oder ? Symbole abschicken! Sonst geht der Post verloren")
        } else if (this.state.minimalPostList.length === 0) {
            Alert.alert("Hinweis", "Poste mindestens einen positiven oder negativen Text um fortzufahren...")
        } else {
            let posts = this.state.minimalPostList;
            let diaryId = this.props?.route?.params.diary.id;
            await posts.forEach(post => {
                createPost(diaryId, post);
            })
            await createDailyMonitoring(diaryId, this.getDailyMonitoringData());
            Navigation.navigate("DiaryTab", "ExperienceOverview")
            this.props.showToast("Tägliche Selbstbeobachtung abgeschlossen!");
        }
    }

    getNextMonitoringDate(){
        if(this.props?.route?.params.diary.daily_monitorings.length == 0){
            let firstDay = new Date();
            let dd = String(firstDay.getDate()).padStart(2, '0');
            let mm = String(firstDay.getMonth() + 1).padStart(2, '0'); //January is 0!
            let yyyy = firstDay.getFullYear();
            firstDay = yyyy + "-" + mm + "-" + dd;
            return firstDay
        } else {
            let dailyMonitorings = this.props?.route?.params.diary.daily_monitorings
            let lastEntry = dailyMonitorings[dailyMonitorings.length -1];
            let dateOfLastEntry = lastEntry.timestamp;
            console.log(dateOfLastEntry)
            var nextDay = new Date(dateOfLastEntry);
            console.log("Next day:" + nextDay)
            nextDay.setDate(nextDay.getDate() + 1);
            let dd = String(nextDay.getDate()).padStart(2, '0');
            let mm = String(nextDay.getMonth() + 1).padStart(2, '0'); //January is 0!
            let yyyy = nextDay.getFullYear();
            nextDay = yyyy + "-" + mm + "-" + dd;
            return nextDay
        }
    }


    getDailyMonitoringData() {
        let diaryData = this.props?.route?.params;
        let dailyMonitoring = {};
        dailyMonitoring.wellbeing = diaryData.wellbeing;
        dailyMonitoring.done = diaryData.challengeCompleted;
        let monitoringDate = this.getNextMonitoringDate();
        dailyMonitoring.timestamp = monitoringDate;
        return dailyMonitoring;
    }

    //get data of random pool for week
    async getRandomHelpText() {
        try {
            let diary = this.props?.route?.params.diary;
            let currentWeek = this.returnCurrentWeek(diary.daily_monitorings); 
            let allDailyHelpTexts = await fetchDailyHelpTexts();
            allDailyHelpTexts = allDailyHelpTexts.filter(helpTexts => helpTexts.week === currentWeek)
            console.log(typeof(allDailyHelpTexts))
            let randomTextItem = allDailyHelpTexts[Math.floor(Math.random()*allDailyHelpTexts.length)];
            this.assureNewText(this.state.help_text, randomTextItem.text)
            this.setState({
                help_text: randomTextItem.text,
                help_button_text: "Andere Reflexionsfrage"
            });
            this.forceUpdateHandler();
        } catch (e) {
            console.log(e);
            this.setState({
                error: JSON.stringify(e),
                loading: false
            });
        }
    }

    assureNewText(oldText, newText){
        if(oldText === newText){
            this.getRandomHelpText()
        }
    }



    onChangeField(fieldName, newValue) {
        this.setState({
            ...this.state,
            [fieldName]: newValue
        });
    }

    ItemSeparator = () => {
        return (
            <View style={styles.itemSeparator}/>
        );
    }

    postListView() {
        if (this.state.minimalPostList.length !== 0) {
            return (
                <ScrollView>
                    <Surface style={styles.card}>
                        <SectionList
                            ItemSeparatorComponent={this.ItemSeparator}
                            sections={this.state.postList}
                            keyExtractor={(item, index) => item + index}
                            renderItem={({item}) => <Text style={styles.item}>{item.text}</Text>}
                            /*renderSectionHeader={({section: {title}}) => (
                                <Text style={styles.sectionHeader}>{title}</Text>
                            )}*/
                        />
                    </Surface>
                </ScrollView>
            )
        }
    }

    addPositivePost() {
        console.log(this.props?.route?.params.diary.daily_monitorings)
        //Add minimalPost for backend
        let minimalPost = {};
        minimalPost.text = this.state.post;
        minimalPost.positive = true;
        this.state.minimalPostList.push(minimalPost);
        //Add post for frontend to postList
        let post = {};
        post.text = "❓: " + this.state.post;
        let postList = this.state.postList;
        postList[0].data.push(post);
        this.setState({...this.state, post: "", postList: postList});
    }

    addNegativePost() {
        //Add minimalPost for backend
        let minimalPost = {};
        minimalPost.text = this.state.post;
        minimalPost.positive = false;
        this.state.minimalPostList.push(minimalPost);
        //Add post for frontend to postList
        let post = {};
        post.text = "❗: " + this.state.post;
        let postList = this.state.postList;
        postList[1].data.push(post);
        this.setState({...this.state, post: "", postList: postList});
    }

    writeDailyPostsView() {
        return (
            <View style={styles.row}>
                <TextInput
                    style={styles.formControl}
                    label="Meine Erfahrung heute…"
                    value={this.state.post}
                    accessibilityLabel="Meine Erfahrung heute…"
                    onChangeText={val => this.onChangeField("post", val)}
                />
                <Button disabled={this.state.post === ""} onPress={() => this.addPositivePost()}><Ionicons
                    style={styles.rightIcon} name={"help"} size={25}/></Button>
                <Button disabled={this.state.post === ""} onPress={() => this.addNegativePost()}><Ionicons
                    style={styles.rightIcon} name={"alert"} size={25}/></Button>
            </View>
        );
    }

    helpTextSection() {
        const diary = this.props?.route?.params.diary;
        CurrentWeek = this.returnCurrentWeek(diary.daily_monitorings);


        return (
            <View style={styles.buttonContainer}>
                <Button style={{flex: 1}} onPress={() => this.getRandomHelpText()}>{this.state.help_button_text}</Button>
            </View>
        );
    }

    render() {
        const dailyObservationText = "Welche positiven und/oder negativen Erfahrungen hast du heute mit dem SelfCommitment gemacht?"
        return (
            <ScrollView style={styles.contentWrapper}>
                <Text style={{textAlign: 'center'}}>2 von 2</Text>
                <ProgressBar progress={1.0} style={{marginBottom: 20}}/>
                <Paragraph>{dailyObservationText}</Paragraph>
                <Divider style={{margin: 15}}/>
                {this.helpTextSection()}
                <Text style={{textAlign: "center",}}>{this.state.help_text}</Text>
                {this.writeDailyPostsView()}
                {this.navigateToExperienceOverviewButton()}
                {this.postListView()}
                <Paragraph style={Typography.descriptionText}>Du kannst die einzelnen Einträge mit ! oder ? taggen, je nach Inhalt.</Paragraph>

                <Paragraph/>
            </ScrollView>);
    }
}

const {width, height} = Dimensions.get("window");
const styles = StyleSheet.create({
    row: {
        display: "flex",
        flexDirection: "row",
        justifyContent: "space-around",
        alignItems: "center"
    },
    formControl: {
        marginVertical: 10,
        width: width / 1.5
    },
    sectionHeader: {
        fontSize: 15,
        fontWeight: 'bold'
    },
    item: {
        fontSize: 14,
        borderWidth: 0.2,
        borderColor: "#f2f2f2",
        borderRadius: 15,
        padding: 3,
        marginVertical: 2,
        backgroundColor: "#f2f2f2"
    },
    buttonContainer: {
        flexDirection: 'row',
        padding: 10
    },
    button: {
        marginHorizontal: 10,
        marginVertical: 10,
        backgroundColor: Colors.green,
    },
    contentWrapper: {
        paddingHorizontal: 10,
        paddingVertical: 10
    },
    card: {
        margin: 10,
        borderRadius: 15,
        display: "flex",
        flexDirection: "column",
        alignItems: "stretch",
        justifyContent: "space-between",
        elevation: 4,
        padding: 5,
    }
});

const mapStateToProps = state => {
    return {};
};

const mapDispatchToProps = dispatch => bindActionCreators({
    showToast
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(DailyReflectionScreen);