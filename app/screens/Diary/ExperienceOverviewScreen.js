import React from "react";
import {FlatList, StyleSheet, View, Dimensions, ScrollView} from "react-native";
import {bindActionCreators} from "redux";
import {Button, Headline, Surface, Text} from "react-native-paper";
import {Ionicons} from "@expo/vector-icons";
import {connect} from "react-redux";
import Navigation from "../../services/Navigation";
import {fetchDiaries} from "../../services/DiaryService";
import {Colors} from "../../styles";

class ExperienceOverviewScreen extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            loading: true,
            index: 0
        };
    }

    componentDidMount() {
        this.subscription = this.props.navigation.addListener("focus", this.reload.bind(this));
        this.reload().then();
    }

    componentWillUnmount() {
        this.subscription();
    }

    async reload() {
        this.setState({loading: true});
        try {
            let diariesData = await fetchDiaries();
            diariesData = diariesData.filter(diary => diary.user.id === this.props.accountId)
            this.setState({
                loading: false,
                diaryParticipations: diariesData
            });
        } catch (e) {
            console.log(e);
            this.setState({
                error: JSON.stringify(e),
                loading: false
            });
        }
    }

    onPressNext = async () => {
        await this.setState({...this.state, index: this.state.index + 1})
        this.flatListRef.scrollToIndex({animated: true, index: this.state.index});
    };

    onPressBack = async () => {
        await this.setState({...this.state, index: this.state.index - 1})
        this.flatListRef.scrollToIndex({animated: true, index: this.state.index});
    };

    render() {
        if (this.state.error) {
            return <View style={styles.container}><Text>{this.state.error}</Text></View>;
        }

        const emptyContent = <Headline style={styles.emptyListWrapper}>Du hast leider noch kein Tagebuch
            offen!</Headline>;

        return (
            <View style={styles.container}>
                <FlatList
                    ref={ref => {
                        this.flatListRef = ref
                    }}
                    horizontal
                    scrollEnabled={false}
                    pagingEnabled={true}
                    showsHorizontalScrollIndicator={false}
                    legacyImplementation={false}
                    testID="my-diaries-list"
                    data={this.state.diaryParticipations}
                    onRefresh={this.reload.bind(this)}
                    refreshing={this.state.loading}
                    keyExtractor={(item) => item.id + ""}
                    ListEmptyComponent={() => emptyContent}
                    renderItem={({item}) =>
                        <DiaryItem
                            participation={item}
                            diaryParticipations={this.state.diaryParticipations}
                            onPressNext={this.onPressNext}
                            onPressBack={this.onPressBack}
                        />
                    }
                />
            </View>);
    }
}

const checkForWeekButton = (dailyMonitorings, week) => {
    switch (week) {
        case 1:
            return (dailyMonitorings.length < 7)
        case 2:
            return (dailyMonitorings.length < 14)
        case 3:
            return (dailyMonitorings.length < 21)
        case 4:
            return (dailyMonitorings.length < 28)
    }
}

const checkForWeeklyMonitoring = (dailyMonitorings, weeklyMonitorings) => {
    //needs to be revised, ugly implementation. Three cases. When seven days have passed, force user to make weeklyMonitoring but stop blocking when it is done. Case 0: allow user to make first entry
    if(dailyMonitorings.length % 7 === 0 && (weeklyMonitorings.length < dailyMonitorings.length/7) && dailyMonitorings.length != 0) return true
    else return false
}

const checkForFinalMonitoring = weeklyMonitorings => {
    return (!(weeklyMonitorings.length >= 4));
}

const getTodaysDate = () => {
    let today = new Date();
    let dd = String(today.getDate()).padStart(2, '0');
    let mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
    let yyyy = today.getFullYear();
    today = yyyy + "-" + mm + "-" + dd;
    return today
}

const checkDailyMonitoring = dailyMonitorings => {
    let result = dailyMonitorings.find(dailyMonitoring => dailyMonitoring.timestamp === getTodaysDate());
    return (result !== undefined);
}

const checkIfMissingEntries = dailyMonitorings => {
        if (dailyMonitorings.length >= 1){
            let today = getTodaysDate();
            var dateToday = new Date(today);
            let lastEntry = dailyMonitorings[dailyMonitorings.length -1];
            let dateOfLastEntry = lastEntry.timestamp;
            var dateLastEntry = new Date(dateOfLastEntry);
            dateLastEntry.setDate(dateLastEntry.getDate() + 1);
            //if unequal, then return true so that if triggers
            //TODO: Better comparison!!!
            return (dateLastEntry.getDate() !== dateToday.getDate());
        } else return false;

}

const checkLastListItem = (diaryParticipations, participation) => {
    let lastListItem = diaryParticipations[diaryParticipations.length -1];
    return (participation === lastListItem);
}

const checkFirstListItem = (diaryParticipations, participation) => {
    let firstListItem = diaryParticipations[0];
    return (participation === firstListItem);
}

function DiaryItem({participation, diaryParticipations, onPressNext, onPressBack}) {
    let dailyButton = () => {
        if (participation.daily_monitorings.length === participation.challenge.duration) {
            return <Button
                style={styles.button}
                mode="contained"
                disabled={true}>
                Fertig
            </Button>
        //update if existing or disabled for now
        } else if (checkDailyMonitoring(participation.daily_monitorings)) {
            return <Button
                style={styles.button}
                mode="contained"
                onPress={() => Navigation.navigate("DiaryTab", "ViewDailyObservation", {diary: participation})}
                disabled={false}>
                Heute <Ionicons style={styles.checkmark} name="ios-checkmark-circle"/>
            </Button>
        }else if (checkIfMissingEntries(participation.daily_monitorings)) {
            return <Button
                style={styles.button}
                mode="contained"
                disabled={true}>
                Bitte nachtragen.
            </Button>
        }
        else if (checkForWeeklyMonitoring(participation.daily_monitorings, participation.weekly_monitorings)) {
            return <Button
                style={styles.button}
                mode="contained"
                disabled={true}>
            Wochenrückblick machen
            </Button>
        }
        //create if not exisiting
        else {
            return <Button
                disabled={checkDailyMonitoring(participation.daily_monitorings)}
                style={styles.button}
                mode="contained"
                onPress={() => Navigation.navigate("DiaryTab", "DailyObservation", {diary: participation})}>
                Heute
            </Button>
        }
    }

    let weeklyButton = (weekNr) => {
        if (participation.weekly_monitorings.find(weeklyMonitoring => weeklyMonitoring.week === weekNr)) {
            return <Button
                style={styles.button}
                mode="contained"
                onPress={() => Navigation.push("DiaryTab", "ViewWeeklyExperienceReport", {
                    diary: participation,
                    week: weekNr
                })}>
                Woche {weekNr} <Ionicons style={styles.checkmark} name="ios-checkmark-circle"/>
            </Button>
        } else {
            return <Button
                disabled={checkForWeekButton(participation.daily_monitorings, weekNr)}
                style={styles.button}
                mode="contained"
                onPress={() => Navigation.push("DiaryTab", "WeeklyExperienceReport", {
                    diary: participation,
                    week: weekNr
                })}>Woche {weekNr}</Button>
        }
    }



    //not in use right now as  correction is not possible yet
    let correctionButton = () => {
        return <Button
            style={styles.button}
            mode="contained"
            onPress={() => Navigation.navigate("DiaryTab", "CorrectMonitoring", {diary: participation})}
            >
            Korrektur
            </Button>
    }

    let enterUpButton = () => {
        //if a week/7 days have passed, the user should be unable to add new entries and first should do the weekly report
        if(checkForWeeklyMonitoring(participation.daily_monitorings, participation.weekly_monitorings)) {
        return <Button
            style={styles.button}
            disabled={true}
            mode="contained"
            onPress={() => Navigation.navigate("DiaryTab", "EnterUpMonitoring", {diary: participation})}
            >
            Wochenrückblick machen
            </Button>
        //if day is today and dailymonitoring is already done or yet to be done today
        } else if (checkDailyMonitoring(participation.daily_monitorings) || !(checkIfMissingEntries(participation.daily_monitorings))) {
            return <Button
                style={styles.button}
                mode="contained"
                //onPress={() => Navigation.navigate("DiaryTab", "UpdateDailyObservation", {diary: participation})}
                disabled={true}>
                Keine verpasssten Tage
            </Button>
        }
        else {
        return <Button
            style={styles.button}
            mode="contained"
            onPress={() => Navigation.navigate("DiaryTab", "EnterUpMonitoring", {diary: participation, enterUpNecessary: checkIfMissingEntries(participation.daily_monitorings)})}
            >
            Tage nachtragen
            </Button>
        }
    }

    let finalButton = () => {
        if (participation.final_monitoring.length == 1) {
            return <Button
                disabled={checkForFinalMonitoring(participation.weekly_monitorings)}
                style={styles.button}
                mode="contained"
                onPress={() => Navigation.navigate("DiaryTab", "ViewFinalReportIntro", {diary: participation})}>
                Rückblick <Ionicons style={styles.checkmark} name="ios-checkmark-circle"/>
            </Button>
        } else {
            return <Button
                disabled={checkForFinalMonitoring(participation.weekly_monitorings)}
                style={styles.button}
                mode="contained"
                onPress={() => Navigation.navigate("DiaryTab", "FinalReportIntro", {diary: participation})}>
                Finaler Rückblick
            </Button>
        }
    }

    return (
        <Surface style={styles.swipeCard}>
            <View style={styles.cardTitle}>
                <View style={{flex: 1}}>
                    <Headline>{participation.challenge.title}</Headline>
                </View>
                <View style={{flex: 0}}>
                    <Text style={styles.category}>{participation.challenge.topic}</Text>
                </View>
            </View>
            <View style={styles.selfCommitmentHeader}>
                <Text style={styles.selfCommitmentHeaderText}>Mein Self Commitment</Text>
            </View>
            <View style={styles.selfCommitment}>
                <Text>{participation.selfCommitment}</Text>
            </View>
            <ScrollView>
                {dailyButton()}
                {weeklyButton(1)}
                {weeklyButton(2)}
                {weeklyButton(3)}
                {weeklyButton(4)}
                {enterUpButton()}
                {finalButton()}
                <View style={styles.participationContent}>
                    <View><Button disabled={checkFirstListItem(diaryParticipations, participation)}
                                  style={styles.nextButton} onPress={onPressBack}>Vorheriges</Button></View>
                    <View><Button disabled={checkLastListItem(diaryParticipations, participation)}
                                  style={styles.nextButton}
                                  onPress={onPressNext}>Nächstes</Button></View>
                </View>
            </ScrollView>
        </Surface>);
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: "row",
    },
    swipeCard: {
        borderRadius: 50,
        elevation: 10,
        padding: 10,
        margin: 30,
        marginTop: 20,
        width: Dimensions.get('window').width - 60,
        height: "92%"
    },
    cardTitle: {
        display: "flex",
        flexDirection: "row",
        alignItems: "flex-start",
        justifyContent: "flex-end"
    },
    category: {
        backgroundColor: Colors.green,
        borderRadius: 10,
        color: "white",
        paddingVertical: 3,
        paddingHorizontal: 10,
    },
    button: {
        marginVertical: 8,
        minWidth: "50%"
    },
    participationContent: {
        flex: 1,
        display: "flex",
        flexDirection: "row",
        alignItems: "flex-end",
        marginTop: 10
    },
    emptyListWrapper: {
        textAlign: "center",
        width: "90%",
        paddingHorizontal: 15,
        marginTop: "25%"
    },
    checkmark: {
        fontSize: 22,
        padding: 0
    },
    nextButton: {
        marginVertical: 4,
        minWidth: "50%"
    },
    selfCommitment: {
        alignItems: "center",
    },
    selfCommitmentHeader: {
        alignItems: "center",
        paddingTop: 10
    },
    selfCommitmentHeaderText: {
        fontWeight: "bold"
    }
});

const mapStateToProps = state => {
    let accountId;
    if (state.account.account.id === undefined) {
        accountId = state.account.account.user_id;
    } else {
        accountId = state.account.account.id;
    }
    return {accountId};
};

const mapDispatchToProps = dispatch => bindActionCreators({}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(ExperienceOverviewScreen);
