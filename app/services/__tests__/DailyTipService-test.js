import {createEmptyTestStore, createNetworkMock} from "../../test/test-utils";
import {fetchDailyTip} from "../DailyTipService";
import {getTodayDate, parseDate} from "../utils";

const mock = createNetworkMock();

describe(`DailyTipService`, () => {
  it(`fetchDailyTip from server`, async () => {
    const store = createEmptyTestStore();
    mock.reset();
    mock.onGet("/tips/daily").reply(200, {
      date: "2000-10-05T22:19:34.550Z",
      title: "Test Tages Tipp",
      content: "TIpp des Tages."
    });

    const dispatch = jest.fn();

    await (fetchDailyTip()(dispatch, store.getState));
    expect(dispatch.mock.calls.length).toBe(1);
    expect(mock.history.get.length).toBe(1);
  });

  it(`fetches daily tip from server when daily tip in store is not older`, async () => {
    const store = createEmptyTestStore();
    mock.reset();
    mock.onGet("/tips/daily").reply(200, {
      date: "2000-10-05T22:19:34.550Z",
      title: "Test Tages Tipp",
      content: "TIpp des Tages."
    });

    store.dispatch({
      type: "RECEIVE_DAILY_TIP",
      dailyTip: {
        date: parseDate("1970-10-05T22:19:34.550Z"),
        title: "Test Tages Tipp",
        content: "TIpp des Tages.",
      }
    });

    const dispatch = jest.fn();

    await (fetchDailyTip()(dispatch, store.getState));
    expect(dispatch.mock.calls.length).toBe(1);
    expect(mock.history.get.length).toBe(1);
  });

  it(`does not refetch daily tip if already in store`, async () => {
    const store = createEmptyTestStore();
    mock.reset();
    store.dispatch({
      type: "RECEIVE_DAILY_TIP",
      dailyTip: {
        date: getTodayDate(),
        title: "Test Tages Tipp",
        content: "TIpp des Tages.",
      }
    });

    const dispatch = jest.fn();

    await (fetchDailyTip()(dispatch, store.getState));
    expect(dispatch.mock.calls.length).toBe(0);
    expect(mock.history.get.length).toBe(0);
  });
});