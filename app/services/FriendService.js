import RemoteStorage from "./RemoteStorage";

const ACCOUNT_API_PATH = "/accounts/";
const FRIENDS_API_PATH = "/account/friends";
const FRIENDREQUEST_API_PATH = "/account/friendrequest";

export async function searchUser(username) {
  return await RemoteStorage.get(ACCOUNT_API_PATH + "?query=" + username);
}

export async function createFriendRequest(userId) {
  return await RemoteStorage.post(FRIENDREQUEST_API_PATH, {receiver: userId});
}

export async function acceptFriendRequest(userId) {
  return await RemoteStorage.put(FRIENDREQUEST_API_PATH + "/" + userId);
}

export async function deleteFriendRequest(userId) {
  return await RemoteStorage.delete(FRIENDREQUEST_API_PATH + "/" + userId);
}

export async function deleteFriend(friendId) {
  return await RemoteStorage.delete(FRIENDS_API_PATH + "/" + friendId);
}

export async function fetchFriends() {
  return await RemoteStorage.get(FRIENDS_API_PATH);
}

export async function fetchOutgoingFriendRequests() {
  return await RemoteStorage.get(FRIENDREQUEST_API_PATH + "?direction=out");
}

export async function fetchIncomingFriendRequests() {
  return await RemoteStorage.get(FRIENDREQUEST_API_PATH + "?direction=in");
}