import RemoteStorage from "./RemoteStorage";

const GROUP_API_PATH = "/groups/";
const GROUP_INVITATION_API_PATH = "/groupInvitations/";
const GROUP_AFFILIATION_PATH = "/groupAffiliations/";



export async function createGroup(groupData) {
  //return await RemoteStorage.post(GROUP_API_PATH, groupData);
  const response = await RemoteStorage.post(GROUP_API_PATH, groupData);
  const group = response.data;
  return group;
}

export async function createGroupInvitation(userId) {
  return await RemoteStorage.post(GROUP_INVITATION_API_PATH, {receiver: userId});
}

export async function createGroupAffiliation(groupAndMemberData, groupId) {
  return await RemoteStorage.post(GROUP_API_PATH + `${groupId}/groupAffiliations/` , groupAndMemberData);
}


export async function acceptFriendRequest(userId) {
  return await RemoteStorage.put(FRIENDREQUEST_API_PATH + "/" + userId);
}

//delete funtions
export async function deleteGroup(groupId) {
  return await RemoteStorage.delete(GROUP_API_PATH + "/" + groupId);
}

export async function deleteGroupAffiliation(groupAffiliationId) {
  return await RemoteStorage.delete(GROUP_AFFILIATION_PATH + "/" + groupAffiliationId);
}

export async function deleteGroupInvitation(groupInvitationId) {
  return await RemoteStorage.delete(GROUP_INVITATION_API_PATH + "/" + groupInvitationId);
}

//fetch data functions
export async function fetchGroups() {
  return await RemoteStorage.get(GROUP_API_PATH);
}


export async function fetchAffiliatedGroups() {
  return await RemoteStorage.get(GROUP_API_PATH + "");
}


export async function fetchGroupInvitations() {
  return await RemoteStorage.get(GROUP_INVITATION_API_PATH);
}


export async function fetchGroupAffiliations() {
  return await RemoteStorage.get(GROUP_AFFILIATION_PATH);
}
