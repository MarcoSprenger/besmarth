import RemoteStorage from "./RemoteStorage";

const SHAREDCONTENT_API_PATH = "/sharing/";
const LIKEDCONTENT_API_PATH = "/sharing/likes";

//SharedContent get and post
export async function createSharedContent(contentData) {
  return await RemoteStorage.post(SHAREDCONTENT_API_PATH, contentData);
}

export async function fetchSharedContent() {
  return await RemoteStorage.get(SHAREDCONTENT_API_PATH);
}

//LikedContent get and post
export async function createLikedContent(contentInstance) {
    return await RemoteStorage.post(LIKEDCONTENT_API_PATH, contentInstance);
}

export async function fetchLikedContent() {
    return await RemoteStorage.get(LIKEDCONTENT_API_PATH);
}

//TODO: UnlikeFunktion!!
/*
export async function deleteLikedContent(contentInstance) {
    return await RemoteStorage.delete(LIKEDCONTENT_API_PATH, contentInstance);
}
*/


