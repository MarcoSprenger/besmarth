import RemoteStorage from "./RemoteStorage";

const DAILY_HELP_TEXT_API_PATH = "/dailyHelpText/";


export async function fetchDailyHelpTexts() {
  const response = await RemoteStorage.get(DAILY_HELP_TEXT_API_PATH);
  return response.data;
}
