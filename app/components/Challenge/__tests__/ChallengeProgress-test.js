import {renderForSnapshot} from "../../../test/test-utils";
import React from "react";
import {ChallengeProgress} from "../ChallengeProgress";

describe("ChallengeProgress", () => {
  it(`renders with one progress`, () => {
    const tree = renderForSnapshot(<ChallengeProgress challenge={{
      id: 134515,
      title: "Challenge Title",
      image: "http://test.local/image/url",
      color: "#abcdef",
      periodicity: "daily",
      duration: 100
    }} participation={{
      joined_time: "2019-12-08T19:20:00.432Z",
      progress: [
        {
          create_time: "2019-12-08T20:21:00.432Z"
        }
      ]
    }}/>).toJSON();
    expect(tree).toMatchSnapshot();
  });

  it(`renders with two progress`, () => {
    const tree = renderForSnapshot(<ChallengeProgress challenge={{
      id: 134515,
      title: "Challenge Title",
      image: "http://test.local/image/url",
      color: "#abcdef",
      periodicity: "daily",
      duration: 100
    }} participation={{
      joined_time: "2019-11-08T19:20:00.432Z",
      progress: [
        {
          create_time: "2019-11-08T20:21:00.432Z"
        },
        {
          create_time: "2019-11-09T20:21:00.432Z"
        }
      ]
    }}/>).toJSON();
    expect(tree).toMatchSnapshot();
  });
});