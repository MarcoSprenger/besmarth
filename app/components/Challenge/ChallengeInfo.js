import {Headline, Paragraph, Divider} from "react-native-paper";
import {Image, StyleSheet, View, Linking} from "react-native";
import React from "react";
import {Typography} from "styles/index"


/**
 * Displays information about a challenge.
 * Is not connected with the redux store
 * Use like <ChallengeInfo challenge={yourChallenge} />
 */
export class ChallengeInfo extends React.Component {

  render() {
    const challenge = this.props.challenge;

    const url = this.getUrlFromText(challenge.description);

    return <View>
      <Image source={{uri: challenge.image}} style={styles.challengeImage} />
      <View style={styles.contentWrapper}>
        <View style={styles.titleWrapper}>
          <Headline style={Typography.descTitle}>{challenge.title}</Headline>
        </View>
        <Paragraph onPress ={() => this.openURL(url)} style={Typography.descriptionText}>{challenge.description}</Paragraph>
      </View>
      <Divider />
    </View>;
  }

  getUrlFromText(text){
    const urlRegex = /(https?:\/\/[^ ]*)/;

    var urlArr = text.match(urlRegex);
    var url = urlArr && urlArr[1];
    return url;
  }

  openURL(url) {
    if(url) {
      Linking.openURL(url);
    }
  }
}



const styles = StyleSheet.create({

  contentWrapper: {
    paddingHorizontal: 8,
    paddingVertical: 4
  },
  challengeImage: {
    height: 150,
    width: "100%"
  }
});
