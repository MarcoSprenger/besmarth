import React from "react";
import {Image, StyleSheet, View} from "react-native";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import Navigation from "../services/Navigation";
import {Subheading, TouchableRipple} from "react-native-paper";
import {Colors} from "../styles/index";

class MapItem extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            loading: true,
        };
    }

    render() {
        const image = this.props.topic.image_level1;
        return (
            <TouchableRipple accessibilityLabel="In Kategorie retten"
                             onPress={() => this.navigateToChallenges()}>
                <View style={styles.mapItem}>
                    <Image source={{uri: image}} style={styles.mapImage}/>
                    <Subheading style={styles.topicTitle}>{this.props.topic.topic_name}</Subheading>
                </View>
            </TouchableRipple>
        );
    }

    navigateToMapItem() {
        this.props.changeCurrentTopic(this.props.topic);
    }

    navigateToChallenges() {
        this.props.changeCurrentTopic(this.props.topic);
        Navigation.push("HomeTab", "Challenges", {topicName: this.props.currentTopicName});
    }
}

const styles = StyleSheet.create({
    mapItem: {
        height: "100%",
        width: "100%",
        position: "relative",
        backgroundColor: Colors.green,
        padding: 2,
        borderRadius: 10,
        elevation: 5
    },
    mapImage: {
        height: "100%",
        width: "100%",
        borderRadius: 10
    },
    topicTitle: {
        alignSelf: "center"
    },
});

const mapStateToProps = state => {
    return {
        currentTopicId: state.topics.currentTopicId,
        currentTopicName: state.topics.currentTopicName,
        currentTopicDescription: state.topics.currentTopicDescription,
        currentTopicImage: state.topics.currentTopicImage
    };
};

const mapDispatchToProps = dispatch => bindActionCreators({
    changeCurrentTopic: (item) => (dispatch2) => dispatch2({type: "CHANGE_CURRENT_TOPIC", item})
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(MapItem);