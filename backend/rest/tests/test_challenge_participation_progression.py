from django.urls import reverse
from rest_framework import status

from .bne_base import BneBaseTest
from ..models import Topic, Challenge, ChallengeProgress, ChallengeParticipation, UserChallenge, SelfCommitment


# ChallengeParticipation and ChallengeProgression are tested in the same class because their
# functionality is intended to work allways together.
class TestChallengeParticipationProgression(BneBaseTest):

    def setUp(self):
        super().setUp()

        tpc1 = Topic(internal_id=1, topic_name='Topic 1', image_level1='img_level1.png')
        tpc2 = Topic(internal_id=2, topic_name='Topic 2', image_level1='img_level2.png')
        selfcommitment, created = SelfCommitment.objects.get_or_create(self_commitment1="", self_commitment2="", self_commitment3="", self_commitment4="", self_commitment5="")
        self.supply_objects(tpc1, tpc2)

        clg1 = UserChallenge(title='Challenge 1', description='Desc 1', icon='icon1.png', image='img1.png', duration=4,
                         color='111111', difficulty=1, periodicity=1, topic=tpc1, selfCommitment=selfcommitment)
        clg2 = UserChallenge(title='Challenge 2', description='Desc 2', icon='icon2.png', image='img2.png', duration=2,
                         color='222222', difficulty=2, periodicity=2, topic=tpc2, selfCommitment=selfcommitment)

        self.supply_objects(clg1, clg2)

    def insert_demo_data(self):
        clg1 = UserChallenge.objects.filter(title='Challenge 1')[0]
        clg2 = UserChallenge.objects.filter(title='Challenge 2')[0]
        self.post_response(reverse('challenge-participation', args=[clg1.pk]))
        self.post_response(reverse('challenge-participation', args=[clg2.pk]))

        # insert progressions
        ChallengeParticipation.objects.filter(user=self.test_user, challenge=clg1)
        ChallengeParticipation.objects.filter(user=self.test_user, challenge=clg1)

        self.post_response(reverse('challenge-progress', args=[clg1.pk]))
        self.post_response(reverse('challenge-progress', args=[clg1.pk]))
        self.post_response(reverse('challenge-progress', args=[clg1.pk]))

        self.post_response(reverse('challenge-progress', args=[clg2.pk]))

        return clg1, clg2

    def validate_participation_response(self, response):
        properties = ['id', 'joined_time', 'challenge', 'mark_deleted']
        for p in properties:
            self.assertTrue(p in response.data, "Response does not contain: {}".format(p))
