from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase

from ..models import Account

TEST_NAME = 'Test Name 122535$ü'
TEST_EMAIL = 'testemail@sustainable-behaviour.com'

class AccountTestCase(APITestCase):
    """ Tests the /account endpoint which is responsible for authenticating and account management """

    def createAccount(self):
        """Convenience method for creating new account"""
        url = reverse('account')
        data = {'pseudonymous': True}
        response = self.client.post(url, data, format='json')
        return response

    def testCreateAccount(self):
        """ Tests if creating an account works"""
        response = self.createAccount()
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(Account.objects.count(), 1)
        self.assertEqual(Account.objects.get().pseudonymous, True)
        self.assertIsNotNone(Account.objects.get().username, True)

    def testCreateAccountThenRegisterThenLogin(self):
        """Tests creating an account, then registering data, then log into that account"""
        url = reverse('account')
        create_response = self.createAccount()
        token = create_response.data['token']

        # Auth against api with newly created account
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + token)

        # test PATCH /account insert data (new username etc.) this is the register process of an user
        new_uname = 'skeeks_testuser'
        data = {'username': new_uname, 'password': '12345', 'first_name': TEST_NAME,
                'email': TEST_EMAIL, 'pseudonymous': False}
        response = self.client.patch(url, data, format='json')

        # check response
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(Account.objects.filter(username=new_uname).count(), 1)
        account = Account.objects.get(username=new_uname)
        self.assertIsNotNone(account)
        self.assertEqual(account.first_name, TEST_NAME)
        self.assertEqual(account.email, TEST_EMAIL)
        self.assertIsNotNone(account.password)
        self.assertNotEqual(account.password, '12345')
        self.assertTrue(account.check_password('12345'))
        self.assertEqual(account.pseudonymous, False)

        # unset credentials from above
        self.client.credentials()

        # test POST /account with username + password
        # Now login into that account with username/password
        data = {'username': new_uname, 'password': '12345'}
        response = self.client.post(url, data, format='json')
        # Check login response
        self.assertEqual(response.data['username'], new_uname)
        self.assertIsNotNone(response.data['token'])
        self.assertEqual(response.data['token'], token)  # Must be same as previous token
        self.assertEqual(response.data['first_name'], TEST_NAME)
        self.assertEqual(response.data['email'], TEST_EMAIL)
        self.assertEqual(response.data['pseudonymous'], False)

        # Login again to test GET /account
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + token)
        get_response = self.client.get(url)
        self.assertEqual(get_response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['first_name'], TEST_NAME)
        self.assertEqual(response.data['email'], TEST_EMAIL)
        self.assertEqual(response.data['pseudonymous'], False)
