from django.urls import reverse
from rest_framework import status
from django.utils import timezone

from .bne_base import BneBaseTest
from ..models import Topic, Challenge, ChallengeParticipation, ChallengeProgress, TopicLevel, UserChallenge, SelfCommitment


class TestTopicProgressView(BneBaseTest):

    def createTopicLevels(self):
        self.topic_level1 = TopicLevel(name="Recruit", limit=10)
        self.topic_level2 = TopicLevel(name="Intermediate", limit=100)
        self.topic_level3 = TopicLevel(name="Expert", limit=1000)
        self.topic_level0 = TopicLevel(name="Max Level", limit=-1)
        self.supply_objects(self.topic_level1, self.topic_level2, self.topic_level3, self.topic_level0)

    def createProgress(self, participation, amount):
        """Create progress for a challenge participation"""
        for _ in range(amount):
            progress = ChallengeProgress(create_time=timezone.now(), mark_deleted=False,
                                         participation_id=participation.id)
            progress.save()

    def createChallenge(self, topic, name, difficulty, duration, progress):
        """
        Creates a challenge as well as challengeparticipation for self.test_user and a certain
        amount of challengeprogress, as specified in `progress` parameter.

        topic      := topic model instance to which this challenge relates
        name       := challenge name
        difficulty := integer value between [0,2]
        duration   := challenge duration (defines the amount of progress until the challenge is finished for once)
        progress   := number of progress to generate
        """
        # color, periodicity and category are irrelevant and therefore statically typed
        selfcommitment, created = SelfCommitment.objects.get_or_create(self_commitment1="", self_commitment2="", self_commitment3="", self_commitment4="", self_commitment5="")
        c = UserChallenge(title=name, duration=duration, color="#FFFFFF",
                      difficulty=Challenge.DIFFICULTY_CHOICES[difficulty][0],
                      periodicity=Challenge.PERIODICITY_CHOICES[0][0],
                      topic=topic,
                      category=Challenge.CATEGORY_CHOICES[0][0], selfCommitment=selfcommitment)
        c.save()
        cp = ChallengeParticipation(joined_time=timezone.now(), shared=False, challenge_id=c.id,
                                    user_id=self.test_user.id, mark_deleted=False)
        cp.save()
        self.createProgress(cp, progress)

    def setUp(self):
        super().setUp()
        # internal_id=0 is defined as "Forest" as of writing of this test. But it does not matter at all,
        # as long as the number is valid/accepted.
        self.topic1 = Topic(internal_id=0, topic_name="Topic1")
        self.topic2 = Topic(internal_id=1, topic_name="Topic2")
        self.supply_objects(self.topic1, self.topic2)


    def test_topic_not_exists(self):
        # presumed 12345 is a non existing topic id
        topic_progress_url = reverse('topic-progress', args=[12345])
        self.get_response(topic_progress_url, exp_status_code=status.HTTP_404_NOT_FOUND)

    def test_no_topic_levels_specified(self):
        """
        Test whether the application manages to deal with a situation where
        the user did not enter any TopicLevels to the database.
        """
        topic_progress_url = reverse('topic-progress', args=[self.topic1.id])
        self.get_response(topic_progress_url)

    def test_first_level(self):
        self.createTopicLevels()

        expected_score = 0
        expected_level = 1
        expected_score_prev = 0
        expected_score_next = 10

        topic_progress_url = reverse('topic-progress', args=[self.topic1.id])
        data = self.get_response(topic_progress_url).data
        self.assertEqual(data['score'], expected_score)
        self.assertEqual(data['level'], expected_level)
        self.assertEqual(data['scorePrevLevel'], expected_score_prev)
        self.assertEqual(data['scoreNextLevel'], expected_score_next)
        self.assertEqual(data['levelDescription'], self.topic_level1.name)
