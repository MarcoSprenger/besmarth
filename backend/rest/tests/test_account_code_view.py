from os.path import abspath, dirname, join

from django.urls import reverse

from .bne_base import BneBaseTest


class TestAccountCodeView(BneBaseTest):
    def test_get(self):
        test_dir = dirname(abspath(__file__))
        test_image = join(test_dir, "test_account_code.png")

        # get AccountCodeView
        account_code_url = reverse('account-code')
        response = self.get_response(account_code_url)

        # Uncomment to generate replace image
        # newFile = open(test_image, "wb")
        # newFileByteArray = bytearray(response.getvalue())
        # newFile.write(newFileByteArray)

        # load file to compare
        file_to_check = open(test_image, "rb")
        self.assertEqual(response.getvalue(), file_to_check.read())
