from datetime import date, timedelta, datetime
from ..models import Diary, DailyMonitoring


# Checks every night at 01:01 if a daily monitoring was made yesterday for a diary
# If no this scripts creates a defined entry in the database
def check_daily_monitoring():
    print(datetime.now.strftime("%d/%m/%Y %H:%M:%S") + ': Started script')
    # Get current date minus 1 day --> yesterday
    current_date = date.today() - timedelta(days=1)

    # Get all diaries from database
    all_diaries = Diary.objects.all()
    print(datetime.now.strftime("%d/%m/%Y %H:%M:%S") + ': All diaries: ' + all_diaries)
    # Get all diary ids
    all_diaries_id = []
    for diary in all_diaries:
        all_diaries_id.append(diary.id)
    print(datetime.now.strftime("%d/%m/%Y %H:%M:%S") + ': All diary ids: ' + all_diaries_id)

    # Filter diaries --> result: All diary ids with no daily monitoring yesterday
    filtered_diaries_id = []
    for diary_id in all_diaries_id:
        if not DailyMonitoring.objects.filter(diary_id=diary_id, timestamp=current_date).exists() and DailyMonitoring.objects.filter(diary_id=diary_id).count() < 28:
            filtered_diaries_id.append(diary_id)
    print(datetime.now.strftime("%d/%m/%Y %H:%M:%S") + ': Filtered diary ids: ' + filtered_diaries_id)

    # Create for every filtered diary id a daily monitoring for yesterday with the same values
    for filtered_diary_id in filtered_diaries_id:
        diary = Diary.objects.get(id=filtered_diary_id)
        DailyMonitoring.objects.create(diary=diary, wellbeing=0.50, done=False, timestamp=current_date)
    print(datetime.now.strftime("%d/%m/%Y %H:%M:%S") + ': Finished script')
