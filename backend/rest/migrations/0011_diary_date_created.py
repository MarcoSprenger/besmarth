# Generated by Django 2.2.6 on 2021-02-21 20:20

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('rest', '0010_auto_20210114_1048'),
    ]

    operations = [
        migrations.AddField(
            model_name='diary',
            name='date_created',
            field=models.DateTimeField(default=django.utils.timezone.now),
        ),
    ]
