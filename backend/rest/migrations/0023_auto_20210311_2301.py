# Generated by Django 2.2.6 on 2021-03-11 22:01

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('rest', '0022_merge_20210310_2355'),
    ]

    operations = [
        migrations.AlterField(
            model_name='dailymonitoring',
            name='date_created',
            field=models.DateTimeField(),
        ),
        migrations.AlterField(
            model_name='dailymonitoring',
            name='timestamp',
            field=models.DateField(),
        ),
        migrations.AlterField(
            model_name='diary',
            name='date_created',
            field=models.DateTimeField(),
        ),
        migrations.AlterField(
            model_name='factor',
            name='date_created',
            field=models.DateTimeField(),
        ),
        migrations.AlterField(
            model_name='finalmonitoring',
            name='date_created',
            field=models.DateTimeField(),
        ),
        migrations.AlterField(
            model_name='hypothesis',
            name='date_created',
            field=models.DateTimeField(),
        ),
        migrations.AlterField(
            model_name='post',
            name='date_created',
            field=models.DateTimeField(),
        ),
        migrations.AlterField(
            model_name='weeklymonitoring',
            name='date_created',
            field=models.DateTimeField(),
        ),
    ]
