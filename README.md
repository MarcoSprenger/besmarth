# IP5 project FHNW - besmarth

[Website](http://86.119.43.108/) | [API](http://86.119.43.108/docs/) | [Wiki](https://gitlab.com/MarcoSprenger/besmarth/-/wikis) | [Dev Env](https://gitlab.com/besmarth/besmarth/-/wikis/Development-Environment)

Besmarth is an Android/iOS App where a community comes together, striving to maintain a clean and sustainable planet.

With Besmarth, you can participate in a variety of given challenges and select a self-commitment that suits you best.
The challenges are available in different topics, such that you can pick just the right one and support what you find most important.
Besmarth provides insights about your chosen self-commitment and how you your well-being changed throughout the weeks. Moreover, you can name personal, social, political and product seller actions which must be implemented for a more sustainable planet.

## Connect to app
Scan the QR-Code on our [Website](http://86.119.43.108/) or open the following link on your smartphone: [exp://86.119.42.242:19000/](exp://86.119.42.242:19000/)

## Wiki

- Documentation and guides are maintained in the [Wiki](https://gitlab.com/MarcoSprenger/besmarth/-/wikis).
- Information on how to set up the development environment can be found here [Dev Env](https://gitlab.com/besmarth/besmarth/-/wikis/Development-Environment)

## Team
- Näf Livio (livio.naef@students.fhnw.ch)
- Sprenger Marco (marco.sprenger@students.fhnw.ch)
